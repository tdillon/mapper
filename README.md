# Mapper

A CLI for creating static Mapbox images using very specific input files.

## Setup

In order to use this application, follow these steps:

- Install [Deno](https://deno.land/)
- Create a [Mapbox token](https://docs.mapbox.com/api/accounts/tokens/) with the following scopes:
  - `styles:list`
  - `styles:read`
  - `styles:tiles`
  - `styles:write`
  - `tilesets:list`
  - `tilesets:read`
  - `tilesets:write`
- Create the following environment variables with the corresponding values:
  - `MAPBOX_TOKEN`
  - `MAPBOX_USERNAME`
- Install/update mapper with the following script:

    ```shell
    deno install --allow-env=MAPBOX_USERNAME,MAPBOX_TOKEN \
                 --allow-read \
                 --allow-write \
                 --allow-net=api.mapbox.com,gitlab.com \
                 --name mapper \
                 --force \
                 https://gitlab.com/api/v4/projects/tdillon%2fmapper/packages/generic/mapper/5.10.11/mapper.js
    ```

- Verify installation:

    ```shell
    mapper version
    ```

- Review `mapper` usage:

    ```shell
    mapper help
    ```

- Create `data.yaml` and `mappings.yaml` (see details below)
- Execute `mapper` commands (read `mapper help` for instructions):

    ```shell
    cd /to/dir/with/yamls
    mapper stats
    mapper
    ```




## data.yaml

This file stores all of the location data you want to map.
Each item is a key/value pair which represents a named set of coordinates.
The coordinates can represent points, lines/roads, and areas/polygons.

The document is a yaml formatted file.
Each key in the file represents the *name* of a map feature(s).
Keys must be unique (behavior is unknown otherwise).
The value for each key is a set of longitude/latitude coordinates.
Individual coordinates are expressed as an array with the ordering being longitude then latitude.
The decimal representation of longitude and latitude can have at most 6 decimal places (e.g., 40.123456).
If an item is expected to be rendered as an `area`, the coordinates must be counter-clockwise (see [RFC 7946](https://datatracker.ietf.org/doc/html/rfc7946])).

In general, the value for lines/roads and areas/polygons will be multiple lon/lat coordinates.

On the other hand, points can be specified individually (e.g., `a single point: [ [ -80, 40 ] ]`) or as a group (e.g., `a lot of points: [ [-80,40], [-80,40.1], [-80,40.2] ]`).
Functionaly, the only difference between these approaches is grouped points are styled together (see `mappings.yaml`).
If you always want a set of points to be styled identically, you should group them in a single item.
If you want the flexibility to only show certain points, or style them differently, you should specify them as individual items.

### Example

A simple `data.yaml` file would be:

```yaml
Center of US: [ [ -103.771556, 44.967243 ] ]
three random points: [[-79,42],[-81,41],[-80,40]]
```

## mappings.yaml

The file will contain an array of the following objects:

```yaml
name: <string>  # Name/description of items being styled.
type: point | line | area  # How are the items rendered?
paint: OPTIONAL - <object>  # See structure below.
objects: <Array<string>>  # List of names from data.yaml.
```

The `paint` data structure for `point` objects is:

```yaml
color: OPTIONAL - <string>  # CSS color string.  Default: #ff7900 (safety orange)
diameter: OPTIONAL - <number>  # The diameter of the point in real world feet.  Default: 3
stroke-color: OPTIONAL - <string>  # CSS color string.  Default: transparent
stroke-width: OPTIONAL - <number>  # Thickness of the stroke around the point in real world feet.  Default: 0
```

The `paint` data structure for `line` objects is:

```yaml
color: OPTIONAL - <string>  # CSS color string.  Default: azure blue
width: OPTIONAL - <number>  # The width of the line in real world feet.  Default: 12
```

The `paint` data structure for `area` objects is:

```yaml
fill: OPTIONAL - <string>  # CSS color string.  Default: #88ff00 (chartreuse)
border: OPTIONAL - <string>  # CSS color string.  Default: transparent
```

### Example

A simple `mappings.yaml` file for the `data.yaml` file above would be:

```yaml
- name: A point I like
  type: point
  paint:
    color: blue
    diameter: 10
  objects:
    - Center of US

- name: random points as a line
  type: line
  paint:
    color: green
    width: 30
  objects:
    - three random points

- name: random points as an area
  type: area
  objects:
    - three random points

- name: all points
  type: point
  paint:
    color: red
    diameter: 4
  objects:
    - three random points
    - Center of US
```

The **Center of US** should only be rendered as a `point` (since there is only 1 coordinate).
You are able to add the **Center of US** item to the map multiple times (potentially with different size/color markers).
On the other hand, **three random points** can be rendered as either a `point`, `line`, or `area`.
Rendering **three random points** as a `point` adds 3 *dots* to the map.
Rendering **three random points** as a `line` would add a line path which begins at the first coordinate and ends at the last.
Rendering **three random points** as an `area` would render a triangle.







## Development

⚠ This info is for `mapper` developers only.

### Releasing

In order to deploy a new release of this application, update the version in `version.ts` and `README.md`.  When the change is made on the `master` branch, it will trigger the deployment jobs.

### Updating the Project Avatar

The `icon-test-job` job creates the project's icon using `mapper.js`.
The files in the `sample-data/icon` directory are used for generating the map.
If changes are desired:

- updated the appropriate YAML files in the `sample-data/icon` directory
- commit/push
- download the image from the `icon-test-job` job's artifact
- upload the image to [General Settings](https://gitlab.com/tdillon/mapper/edit)

### Deleting a Release

If cleanup is needed, releases in GitLab must be deleted using the API.

```shell
curl --header "PRIVATE-TOKEN: <TOKEN_WITH_API_PERM>" -X DELETE https://gitlab.com/api/v4/projects/26122040/releases/<TAG>
```

### Updating Dependencies

Add/update the dependency in `deps.ts` and then run the following command.

```shell
deno task cache-update
```
