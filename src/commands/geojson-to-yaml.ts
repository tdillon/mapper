import { MapperArgs } from '/src/utils/mapper-args.ts'
import { GeoJSON, LonLatTuple } from '/src/utils/types.ts'

/**
 * Find all GeoJSON files in `<root>`.
 * Enumerate each GeoJSON file and create a 'Segment' in YAML format for each feature.
 *
 * @param args Command line arguments.
 * @returns A string representation of `data.yaml` created from the GeoJSON files.
 */
export function geoJSONToYaml(args: MapperArgs): string {
    /** Container for YAML data to be output.  Populated from GeoJSON files */
    const data: Array<string> = [];

    for (const e of Deno.readDirSync(args.root)) {
        if (e.isFile && e.name.toLowerCase().endsWith('.geojson')) {
            let geoJSON: GeoJSON

            try {
                geoJSON = JSON.parse(Deno.readTextFileSync(`${args.root}/${e.name}`))
            } catch {
                throw new Error('Something is not right with your GeoJSON files.')
            }

            data.push(...geoJSON.features.flatMap(f => {
                let coordinates: Array<LonLatTuple>

                switch (f.geometry.type) {
                    case "Point":
                        coordinates = [f.geometry.coordinates]
                        break;
                    case "Polygon":
                        if (f.geometry.coordinates.length > 1) {
                            throw new Error('mapper does not currently support holes in polygons')
                        }
                        coordinates = f.geometry.coordinates[0]
                        break
                    case "MultiPoint":
                    case "LineString":
                        coordinates = f.geometry.coordinates
                        break
                    default:
                        throw new Error(`Your GeoJSON has an unexpected geometry type: [${f.geometry['type']}]`)
                }

                return `${f.properties.dataName ?? f.properties.name}: ${JSON.stringify(coordinates)}`
            }))
        }
    }

    if (data.length === 0) {
        throw new Error('\nNo GeoJSON file found.\n')
    }

    return data.join('\n')
}
