import { MapperArgs } from '/src/utils/mapper-args.ts'
import { VERSION } from "/src/commands/version.ts"

// Loosely use http://docopt.org/ for formatting.

export const HELP = `
mapper ${VERSION}

A CLI for creating static Mapbox images using very specific input files.

Powered by:
  © Mapbox https://www.mapbox.com/about/maps/
  © OpenStreetMap http://www.openstreetmap.org/copyright

Source: https://gitlab.com/tdillon/mapper


Usage:
  mapper [--root=<root>]
         [--data=<file>]
         [--mappings=<file>]
         [--prefix=<prefix>]
         [--basemap=<map>]...
         [--size=<WIDTHxHEIGHT[xZOOM]>]...
         [--ratio=<WIDTHxHEIGHT[xZOOM]>]...
         [--padding=<num>]
  mapper (help | version | upgrade)
  mapper gpx-to-yaml [--root=<root>]
  mapper stats [--root=<root>]
               [--data=<file>]
               [--mappings=<file>]
  mapper simplify [--root=<root>]
                  [--data=<file>]
                  [--mappings=<file>]
                  [--offset=<num>]
  mapper geojson-to-yaml [--root=<root>]
  mapper yaml-to-geojson [--root=<root>]
                         [--data=<file>]
                         [--mappings=<file>]


Commands:
  [NO COMMAND]
    If no command is specified, the default behavior is used.  The default
    behavior is to generate images based on the options.

  help
    Show this help message and exit.

  gpx-to-yaml
    Read all GPX files in the input directory and print them out in data.yaml
    format.

  geojson-to-yaml
    Read all GeoJSON files in the input directory and print them out in
    data.yaml format.

  yaml-to-geojson
    Output a GeoJSON file based on your data and mappings files.

  stats
    Output distances and areas of your data file.  The current implementation
    assumes map data is contained within a few hundred acres.  Otherwise,
    results are unknown.

  simplify
    Output all lines in your mapping file after being simplified.  The output
    is in data file format.

  upgrade
    Check if there is a newer version of mapper.  Installation instructions
    are output if a newer version is available.

  version
    Show version and exit.


Options:
  --root <root>
      All output will be written to this directory. Input files use this
      directory by default.  Can be relative or absolute.

      [default: ${MapperArgs.DEFAULT_ROOT}]

  --data <file>
      A YAML file containing the raw coordinate data.  For a detailed
      explanation of the file see:
      https://gitlab.com/tdillon/mapper/-/blob/master/README.md

      [default: <root>/data.yaml]

  --mappings <file>
      A YAML file containing the "mappings" between the raw coordinate data and
      the resultant map image.  For a detailed explanation of the file see:
      https://gitlab.com/tdillon/mapper/-/blob/master/README.md

      [default: <root>/mappings.yaml]

  --prefix <prefix>
      This prefix is added to all files created as well as Mapbox objects
      created.  This allows mapper to be run multiple times without
      conflicts.

      Example: mapper --mappings=a.yaml --prefix=fancy
               mapper --mappings=b.yaml --prefix=plain

      [default: kabob-case of <root>]

  --basemap <map>
      The underlying map to which your coordinate data is added.

      The accepted values are:
          ${MapperArgs.ALLOWED_BASEMAP_ARGS.join('\n          ')}

      [default: ${MapperArgs.DEFAULT_BASEMAP.map(b => [...b.matchAll(/(.+)-v[0-9]*/g)][0][1]).sort().join(', ')}]

  --size <WIDTHxHEIGHT[xZOOM]>
      The size of the output image.  The width and height values (as
      integers) separated by an x.  The maximum allowed value for either
      dimension is 2560.  Once either dimension is larger than 1280, they must
      both be even.

      Example: --size=480x800 would yield an image 480px wide by 800px tall.

      Optionally you can append a value for the zoom to use when rendering the
      image.  The value must be between 0 and 22.  If the zoom is ommitted the
      default behavior is to zoom to the bounding box.

      Example: --size=480x800x16.5 would yield an image with a zoom of 16.5.

      This flag can be added multiple times to produce images for each size
      provided.  The default is only used if both --size and --ratio are not
      provided.

      [default: ${MapperArgs.DEFAULT_SIZE.map(s => `${s.toString(false)}`).join(', ')}]

  --ratio <WIDTHxHEIGHT[xZOOM]>
      The ratio of the dimensions of the output image.  The width and height
      values (as decimals) separated by an x.  The output image dimensions are
      scaled up to the maximum allowed values to produce the desired ratio.

      Example: --ratio=2x1 would yield an image 2560px wide by 1280px tall.

      Optionally you can append a value for the zoom to use when rendering the
      image.  The value must be between 0 and 22.  If the zoom is ommitted the
      default behavior is to zoom to the bounding box.

      Example: --ratio=1x1x14.5 would yield an image with a zoom of 14.5.

      This flag can be added multiple times to produce images for each ratio
      provided.  The default is only used if both --size and --ratio are not
      provided.

      [default: ${MapperArgs.DEFAULT_RATIO.map(r => `${r.toString(false)}`).join(', ')}]

  --padding <num>
      The amount of padding (in pixels) to surround your map data with when
      rendering a bounding box map.

      [default: ${MapperArgs.DEFAULT_PADDING}]

  --offset <num>
      When simplifying lines, this <num> is the amount of feet for which
      coordinates inside a stright <num> feet wide will be removed.

      [default: ${MapperArgs.DEFAULT_OFFSET}]


Examples:
  This is the simplest way to run mapper.  Uses all defaults.
  $ mapper

  This is a kitchen sink example to show all options being used.
  $ mapper --root /tmp/working-dir
           --data ../myData.yaml
           --mappings aFolder/myMappings.yaml
           --prefix foo-bar
           --basemap outdoors
           --basemap light
           --basemap dark
           --size 600x600
           --size 480x640x15.5
           --ratio 8.5x11
           --ratio 7x5x15.5
           --padding 10

  Output data from all GPX files in the root directory in data.yaml format.
  $ mapper gpx-to-yaml

  Output data from all GeoJSON files in the root directory in data.yaml format.
  $ mapper geojson-to-yaml

  Output GeoJSON file for the default data and mappings files.
  $ mapper yaml-to-geojson

  Output length and area details for the default data and mappings files.
  $ mapper stats

  Output simplified lines using the default data and mappings files.
  $ mapper simplify

  Outputs lines simplified by 10ft using the default data and mappings files.
  $ mapper simplify --offset 10


Environment Variables:
  MAPBOX_USERNAME    Mapbox username associated with the API access token.
  MAPBOX_TOKEN       API access token used for accessing the Mapbox API. See
                     https://gitlab.com/tdillon/mapper/-/blob/master/README.md
                     for access token requirements.


File Specifications:
  The 2 main input files for mapper are the "data" file and "mappings" file.
  See https://gitlab.com/tdillon/mapper/-/blob/master/README.md for a complete
  specification of the data and mappings files.  Below is a sample of each
  file to quickly give you an idea of the structure.

  # data.yaml
  Center of US: [[-103.771556,44.967243]]
  three random points: [[-79,42],[-81,41],[-80,40]]

  # mappings.yaml - (paint and all of its options are optional)
  - name: all points
    type: point
    paint:
      color: blue
      diameter: 10
      stroke-color: white
      stroke-width: 5
    objects:
      - three random points
      - Center of US

  - name: random points as a line
    type: line
    paint:
      color: green
      width: 30
    objects:
      - three random points

  - name: random points as an area
    type: area
    paint:
      fill: orange
      border: lime
    objects:
      - three random points
`
