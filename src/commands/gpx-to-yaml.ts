import { parse } from 'xml'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { LonLatTuple } from '/src/utils/types.ts'

/** Structure of the trkpt element in GPX files we care about. */
interface trkpt {
    '@lat': number
    '@lon': number
}

/** Structure of GPX files we care about. */
interface GPXFile {
    gpx: {
        trk: {
            trkseg: Array<{ trkpt: Array<trkpt> | trkpt }> |
            { trkpt: Array<trkpt> | trkpt }
        }
    }
}

/**
 * Find all gpx files in `<root>`.
 * Enumerate each gpx file and create a 'Segment' in YAML format for each `trkseg` element.
 *
 * @param args Command line arguments.
 * @returns A string representation of `data.yaml` created from the gpx files.
 */
export function gpxToYaml(args: MapperArgs): string {
    /** Container for YAML data to be output.  Populated from GPX files */
    const data: Array<Array<LonLatTuple>> = [];

    try {
        for (const e of Deno.readDirSync(args.root)) {
            if (e.isFile && e.name.toLowerCase().endsWith('.gpx')) {
                const gpxContent = Deno.readTextFileSync(`${args.root}/${e.name}`)
                const px = parse(gpxContent) as unknown as GPXFile

                // If there is a single trkseg, turn it into an array.
                if (!('length' in px.gpx.trk.trkseg)) {
                    px.gpx.trk.trkseg = [px.gpx.trk.trkseg]
                }

                data.push(...px.gpx.trk.trkseg.map(s => {
                    // If there is a single trkpt, turn it into an array.
                    if (!('length' in s.trkpt)) {
                        s.trkpt = [s.trkpt]
                    }

                    return s.trkpt.map(k => [k['@lon'], k['@lat']] as LonLatTuple)
                }))
            }
        }
    } catch {
        throw new Error('Something is not right with your GPX files.')
    }

    if (data.length === 0) {
        throw new Error('\nNo GPX file found.\n')
    }

    return data.map((t, idx) => `Segment ${idx}: ${JSON.stringify(t)}`).join('\n')
}
