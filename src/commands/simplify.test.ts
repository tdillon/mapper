import { assert } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { simplify } from '/src/commands/simplify.ts'

Deno.test('simplify', async t => {
    // BUG test that output does not contain 'NaN ft' -> line: [[-90,40],[-90.00005,40],[-90.0001,40]]

    await t.step('simplify point and line', () => {
        const dataAndMappingsContentStub = stub(Deno, 'readTextFileSync', returnsNext([
            'point: [[-90,40]]',
            `- name: Point\n  type: point\n  objects: [point]`,
            'line: [[-90,40],[-90.00005,40],[-90.0001,40]]',
            `- name: Line\n  type: line\n  objects: [line]`,
        ]))
        try {
            assert(simplify({} as MapperArgs).length === 0)
            assert(simplify({} as MapperArgs).length > 0)
        } finally {
            dataAndMappingsContentStub.restore()
        }
    })
})
