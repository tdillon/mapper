import { assertMatch } from 'std/assert/mod.ts'
import { VERSION } from '/src/commands/version.ts'

Deno.test('the version uses semantic versioning format', () => {
    // See https://semver.org/ for RegExp used.
    assertMatch(VERSION, /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/, 'Version must be semantic versioned -> https://semver.org/')
})