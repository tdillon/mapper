import { getDataFileContent, getGeoJSONContent, getMappingsFileContent } from '/src/utils/helpers.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'

export function yamlToGeoJSON(args: MapperArgs): string {
    const dataFileContent = getDataFileContent(args.dataFileFullPath)
    const mappingsFileContent = getMappingsFileContent(args.mappingsFileFullPath, dataFileContent)
    const geoJSONContent = getGeoJSONContent(dataFileContent, mappingsFileContent)

    return JSON.stringify(geoJSONContent)
}
