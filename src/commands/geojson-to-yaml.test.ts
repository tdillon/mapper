import { assertStrictEquals, assertThrows } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { geoJSONToYaml } from '/src/commands/geojson-to-yaml.ts'

Deno.test('geojson-to-yaml', async (t) => {

  const readDirSyncIterable: Iterable<Deno.DirEntry> = { [Symbol.iterator]: function* () { } };

  readDirSyncIterable[Symbol.iterator] = function* () {
    yield {
      isDirectory: false,
      isFile: true,
      isSymlink: false,
      name: 'bob.GeoJSON',
    }
  }

  const validGeoJSONToYamlTestCases = [
    [`{
        "type": "FeatureCollection",
        "features": [{
          "geometry":{"type":"MultiPoint","coordinates":[[-80.1,40.2],[-80.3,40.4]]},
          "properties":{"name":"Use dataName property","dataName":"Segment 0"},
          "type": "Feature"
        }]
      }`,
      'Segment 0: [[-80.1,40.2],[-80.3,40.4]]',
      'Simple GeoJSON with dataName property'
    ], [`{
        "type": "FeatureCollection",
        "features": [{
          "geometry":{"type":"LineString","coordinates":[[-80.1,40.2],[-80.3,40.4]]},
          "properties":{"name":"Name Property Only"},
          "type":"Feature"
        }]
      }`,
      'Name Property Only: [[-80.1,40.2],[-80.3,40.4]]',
      'Simple GeoJSON without dataName property'
    ], [`{
        "type": "FeatureCollection",
        "features": [{
          "geometry":{"type":"Point","coordinates":[-80.1,40.2]},
          "properties":{"name":"Only a single lonlat tuple"},
          "type":"Feature"
        }]
      }`,
      'Only a single lonlat tuple: [[-80.1,40.2]]',
      'GeoJSON with single lonlat tuple without dataName property'
    ], [`{
       "type":"FeatureCollection",
       "features": [{
            "geometry":{"type":"Point","coordinates":[1.5,2.7]},
            "properties":{"name":"Point"},
            "type":"Feature"
          },{
            "geometry":{"type":"MultiPoint","coordinates": [[1.3,2.7],[1.4,2.8],[1.5,2.9]]},
            "properties":{"name":"MultiPoint"},
            "type":"Feature"
          },{
            "geometry":{"type":"LineString","coordinates": [[1.96,2.8],[1.5,2.6],[1.4,2.7]]},
            "properties":{"name":"Line"},
            "type":"Feature"
          },{
            "geometry":{"type":"Polygon","coordinates":[[[1.4,2.8],[1.4,2.9],[1.3,2.9],[1.4,2.8]]]},
            "properties":{"name":"Polygon"},
            "type":"Feature"
          }
        ]
      }`,
      'Point: [[1.5,2.7]]\nMultiPoint: [[1.3,2.7],[1.4,2.8],[1.5,2.9]]\nLine: [[1.96,2.8],[1.5,2.6],[1.4,2.7]]\nPolygon: [[1.4,2.8],[1.4,2.9],[1.3,2.9],[1.4,2.8]]',
      'Point, MultiPoint, LineString, and Polygon'
    ]
  ]

  for (const tc of validGeoJSONToYamlTestCases) {
    await t.step(tc[2], () => {
      const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
      const readTextFileSyncStub = stub(Deno, 'readTextFileSync', returnsNext(tc))
      try {
        assertStrictEquals(geoJSONToYaml({ root: './' } as MapperArgs), tc[1])
      } finally {
        readDirSyncStub.restore()
        readTextFileSyncStub.restore()
      }
    })
  }

  await t.step('No GeoJSON file exists', () => {
    assertThrows(() => geoJSONToYaml({ root: './' } as MapperArgs), Error, 'No GeoJSON file found.')
  })

  await t.step('Invalid GeoJSON file', () => {
    const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
    const readTextFileSyncStub = stub(Deno, 'readTextFileSync', returnsNext(['{foo:"bad geojson file"}']))
    try {
      assertThrows(() => geoJSONToYaml({ root: './' } as MapperArgs), Error, 'Something is not right with your GeoJSON files.')
    } finally {
      readDirSyncStub.restore()
      readTextFileSyncStub.restore()
    }
  })

  await t.step('Polygon with a hole', () => {
    const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
    const readTextFileSyncStub = stub(Deno, 'readTextFileSync', returnsNext([`{
      "type":"FeatureCollection",
      "features": [{
        "geometry":{"type":"Polygon","coordinates":[[[1.2,3.1],[1.4,2.6],[1.5,3.1],[1.2,3.1]],[[1.4,2.8],[1.3,2.9],[1.4,2.9],[1.4,2.8]]]},
        "properties":{"name":"Polygon with a hole"},
        "type":"Feature"
      }]
    }`]))
    try {
      assertThrows(() => geoJSONToYaml({ root: './' } as MapperArgs), Error, 'mapper does not currently support holes in polygons')
    } finally {
      readDirSyncStub.restore()
      readTextFileSyncStub.restore()
    }
  })

  await t.step('GeoJSON with an unexpected geometry type', () => {
    const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
    const readTextFileSyncStub = stub(Deno, 'readTextFileSync', returnsNext([`{
      "type":"FeatureCollection",
      "features": [{
        "geometry":{"type":"FutureType","coordinates":[[[[[[[1,2,3]]]]]]]},
        "properties":{"name":"Fancy Future Thing"},
        "type":"Feature"
      }]
    }`]))
    try {
      assertThrows(() => geoJSONToYaml({ root: './' } as MapperArgs), Error, 'Your GeoJSON has an unexpected geometry type: [')
    } finally {
      readDirSyncStub.restore()
      readTextFileSyncStub.restore()
    }
  })

})