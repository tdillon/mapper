import { default as CheapRuler } from 'CheapRuler'
import { default as geoSimplify } from 'geoSimplify'
import { sprintf } from 'std/fmt/printf.ts'
import { getDataFileContent, getMappingsFileContent } from '/src/utils/helpers.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { DataYaml, LonLatTuple, MappingsThing } from '/src/utils/types.ts'


/** Return a string in data.yaml format after input files have been simplified using geoSimplify. */
export function simplify(args: MapperArgs): string {
    const dataFileContent: DataYaml = getDataFileContent(args.dataFileFullPath)
    const mappingsFileContent: Array<MappingsThing> = getMappingsFileContent(args.mappingsFileFullPath, dataFileContent)

    /** Contains each line of output to be returned. */
    const data: Array<string> = []

    const uniqueLineObjectNames = Array.from(
        new Set(mappingsFileContent.filter(m => m.type === 'line').flatMap(ml => ml.objects))
    )

    for (const name of uniqueLineObjectNames) {
        const simplified: Array<LonLatTuple> = geoSimplify(dataFileContent[name], args.offset)
        if (simplified.length !== dataFileContent[name].length) {
            const ruler = new CheapRuler(dataFileContent[name][0][1], 'feet')
            const origLength = ruler.lineDistance(dataFileContent[name])
            const simplifiedLength = ruler.lineDistance(simplified)

            const col1len = dataFileContent[name].length.toString().length
            const col2len = origLength.toFixed(0).length

            data.push(`# ${name}`)

            // e.g., #       offset: 1 ft
            data.push(`#       offset: ${args.offset} ft`)

            // e.g., #     original: 82 coords - 385 ft
            data.push(`#     original: ${dataFileContent[name].length} coords - ${origLength.toFixed(0)} ft`)

            // e.g., #   simplified:  8 coords - 380 ft
            data.push(sprintf(`#   simplified: %*d coords - %*d ft`,
                col1len,
                simplified.length,
                col2len,
                simplifiedLength.toFixed(0)))

            // e.g., #   difference: 74 coords -   5 ft
            data.push(sprintf('#   difference: %*d coords - %*d ft',
                col1len,
                dataFileContent[name].length - simplified.length,
                col2len,
                (origLength - simplifiedLength).toFixed(0)))

            // e.g., #    reduction: 90.2%     -   1.2%
            data.push(sprintf('#    reduction: %*.1f%%     - %*.1f%%',
                col1len + 2,
                (1 - (simplified.length / dataFileContent[name].length)) * 100,
                col2len + 2,
                (1 - (simplifiedLength / origLength)) * 100))

            data.push(`${name}: ${JSON.stringify(simplified)}`)
            data.push('')
        }
    }

    return data.join('\n')
}
