import { VERSION } from '/src/commands/version.ts'

interface GitLabGraphQLReleseResponse {
    data: {
        project: {
            releases: {
                nodes: Array<{
                    name: string,
                    description: string
                }>
            }
        }
    }
}

/**
 * @example
 * console.log(await getUpgradeText())
 */
export async function getUpgradeText() {
    const res = await fetch(`https://gitlab.com/api/graphql`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
        body: JSON.stringify({
            query: `{
                project(fullPath: "tdillon/mapper") {
                    releases(first: 1, sort: CREATED_DESC) {
                        nodes {
                            name
                            description
                        }
                    }
                }
            }`
        })
    })

    if (res.status !== 200) {
        throw new Error(await res.text())
    }

    const data = <GitLabGraphQLReleseResponse>(await res.json())
    const latestVersion = data.data.project.releases.nodes[0].name.substring(1)
    const description = data.data.project.releases.nodes[0].description

    if (VERSION === latestVersion) {
        return `Local mapper version ${VERSION} is the most recent release`
    } else {
        return `Local mapper version ${VERSION}
Latest mapper version ${latestVersion}

Run the following command to upgrade

${description}`
    }
}
