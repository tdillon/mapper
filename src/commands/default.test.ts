import { assertEquals, assertRejects, assertStrictEquals } from 'std/assert/mod.ts'
import { parseArgs } from 'std/cli/parse_args.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import Default from '/src/commands/default.ts'
import { USERNAME } from '/src/utils/config.ts'
import { ImageSize } from '/src/utils/image-size.ts'
import { Mapbox } from '/src/utils/mapbox.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { getTilesetSourceID } from '/src/utils/template-recipe.ts'
import { RetrieveAStyleResponse } from '/src/utils/types.ts'

Deno.test('default.ts', async t => {
  const myStub = stub(Deno, 'readTextFileSync', returnsNext([
    'a: [[0,0],[1,1],[2,2]]',
    `
- name: foo
  type: point
  objects:
    - a
  paint:
    color: red
    diameter: 1
    stroke-color: red
    stroke-width: 1
- name: bar
  type: line
  paint:
    color: red
    width: 1
  objects:
    - a
- name: baz
  type: area
  paint:
    fill: red
    border: red
  objects:
    - a`
  ]))

  let a: MapperArgs
  let d: Default

  try {
    a = new MapperArgs(parseArgs([]))
    d = new Default(a)
  } finally {
    myStub.restore()
  }

  await t.step('styleFileFullPath', () => {
    const style = 'dark-v11'
    assertStrictEquals(d['styleFileFullPath'](style), `${a.root}/${a.prefix}-${style}-style.json`)
  })

  await t.step('printInfo', () => {
    assertStrictEquals(d['printInfo'](), undefined)
  })

  await t.step('getBoundingBox', () => {
    assertEquals(d['getBoundingBox'](), [0, 0, 2, 2])
  })

  await t.step('getCenter', () => {
    assertStrictEquals(d['getCenter'](), '1,1')
  })

  await t.step('createTileset', async () => {
    const aStub = stub(Mapbox, 'createTileset', returnsNext([new Promise(res => res(undefined))]))
    try {
      assertStrictEquals(await d['createTileset'](), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('publishTileset', async () => {
    const aStub = stub(Mapbox, 'publishTileset', returnsNext([new Promise(res => res('TEST'))]))
    try {
      assertStrictEquals(await d['publishTileset'](), 'TEST')
    } finally {
      aStub.restore()
    }
  })

  await t.step('deleteStyle', async () => {
    const aStub = stub(Mapbox, 'deleteStyle', returnsNext([new Promise(res => res(undefined))]))
    try {
      assertStrictEquals(await d['deleteStyle']('TEST'), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('deleteTileset', async () => {
    const aStub = stub(Mapbox, 'deleteTileset', returnsNext([new Promise(res => res(undefined))]))
    try {
      assertStrictEquals(await d['deleteTileset'](), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('deleteTilesetSource', async () => {
    const aStub = stub(Mapbox, 'deleteTilesetSource', returnsNext([new Promise(res => res(undefined))]))
    try {
      assertStrictEquals(await d['deleteTilesetSource'](), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('createStyle', async () => {
    const aStub = stub(Mapbox, 'createStyle', returnsNext([new Promise(res => res('TEST'))]))
    const bStub = stub(Deno, 'readTextFile', returnsNext([new Promise(res => res('TEST'))]))

    try {
      assertStrictEquals(await d['createStyle']('dark-v11'), 'TEST')
    } finally {
      aStub.restore()
      bStub.restore()
    }
  })

  await t.step('getImage', async () => {
    const aStub = stub(Mapbox, 'getImage', returnsNext([
      new Promise(res => res(new Uint8Array([0]))),
      new Promise(res => res(new Uint8Array([0])))
    ]))
    const bStub = stub(Deno, 'writeFile', returnsNext([
      new Promise(res => res(undefined)),
      new Promise(res => res(undefined))
    ]))
    try {
      assertStrictEquals(await d['getImage']('myStyleId', 'dark-v11', ImageSize.fromRatio(1, 1)), undefined)
      assertStrictEquals(await d['getImage']('myStyleId', 'dark-v11', ImageSize.fromSize(1, 1)), undefined)
    } finally {
      aStub.restore()
      bStub.restore()
    }
  })

  await t.step('waitForJobToFinish', async () => {
    const aStub = stub(Mapbox, 'getJobStatus', returnsNext([
      new Promise(res => res('success'))
    ]))

    try {
      // TODO can we override delay?  adds 20 seconds to tests
      assertStrictEquals(await d['waitForJobToFinish']('myStyleId', 0), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('createTilesetSource', async () => {
    const aStub = stub(Mapbox, 'createTilesetSource', returnsNext([new Promise(res => res(undefined))]))

    try {
      assertStrictEquals(await d['createTilesetSource'](), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('createGeoJSONFile', async () => {
    const aStub = stub(Deno, 'writeTextFileSync', returnsNext([new Promise(res => res(undefined))]))

    try {
      assertStrictEquals(await d['createGeoJSONFile'](), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('createNDJsonFile', async () => {
    const aStub = stub(Deno, 'writeTextFileSync', returnsNext([undefined]))

    try {
      assertStrictEquals(await d['createNDJsonFile'](), undefined)
    } finally {
      aStub.restore()
    }
  })

  await t.step('deleteStyles', async () => {
    const aStub = stub(Mapbox, 'getAllStyles', returnsNext([
      new Promise(res => res([])),
      new Promise(res => res([{ id: 'id', name: ' - mapper Style' }]))
    ]))
    const bStub = stub(Mapbox, 'deleteStyle', returnsNext([
      new Promise(res => res(undefined)),
      new Promise(res => res(undefined))
    ]))

    try {
      assertStrictEquals(await d['deleteStyles'](), undefined)
      assertStrictEquals(await d['deleteStyles'](), undefined)
    } finally {
      aStub.restore()
      bStub.restore()
    }
  })

  await t.step('deleteTilesets', async () => {
    const aStub = stub(Mapbox, 'getAllTilesets', returnsNext([
      new Promise(res => res([])),
      new Promise(res => res([`${USERNAME}.${d['args2'].prefix}`])),
      new Promise(res => res(['foo']))
    ]))
    const bStub = stub(Mapbox, 'deleteTileset', returnsNext([
      new Promise(res => res(undefined)),
      new Promise(res => res(undefined))
    ]))

    try {
      assertStrictEquals(await d['deleteTilesets'](), undefined)
      assertStrictEquals(await d['deleteTilesets'](), undefined)
      assertStrictEquals(await d['deleteTilesets'](), undefined)
    } finally {
      aStub.restore()
      bStub.restore()
    }
  })

  await t.step('createStyleFile', async () => {
    const aStub = stub(Mapbox, 'getStyle', returnsNext([new Promise(res => res(
      {
        name: '',
        sources: { composite: { url: 'test', type: 'test' } },
        layers: [{ id: 'test' }]
      } as RetrieveAStyleResponse)),
    new Promise(res => res(
      {
        name: '',
        sources: { composite: { url: 'test', type: 'test' } },
        layers: [{ id: 'contour-label', layout: {} }]
      } as RetrieveAStyleResponse))]))
    const bStub = stub(Deno, 'writeTextFileSync', returnsNext([
      new Promise(res => res(undefined)),
      new Promise(res => res(undefined))
    ]))

    try {
      assertStrictEquals(await d['createStyleFile']('dark-v11'), undefined)
      assertStrictEquals(await d['createStyleFile']('satellite-v9'), undefined)
    } finally {
      aStub.restore()
      bStub.restore()
    }
  })

  await t.step('deleteFile', async () => {
    const aStub = stub(Deno, 'remove', returnsNext([
      new Promise(res => res(undefined)),
      new Promise((_res, rej) => rej(new Deno.errors.NotFound('TEST'))),
      new Promise((_res, rej) => rej(new Error('TEST')))
    ]))

    try {
      assertStrictEquals(await d['deleteFile']('exists.txt'), undefined)
      assertStrictEquals(await d['deleteFile']('non-existant.txt'), undefined)
      assertRejects(async () => await d['deleteFile']('exception.txt'), Error, 'TEST')
    } finally {
      aStub.restore()
    }
  })

  await t.step('deleteTilesetSources', async () => {
    const aStub = stub(Mapbox, 'getAllTilesetSources', returnsNext([
      new Promise(res => res([])),
      new Promise(res => res([''])),
      new Promise(res => res([getTilesetSourceID(d['args2'].prefix, USERNAME)]))
    ]))
    const bStub = stub(Mapbox, 'deleteTilesetSource', returnsNext([
      new Promise(res => res(undefined)),
      new Promise(res => res(undefined)),
      new Promise(res => res(undefined))
    ]))

    try {
      assertStrictEquals(await d['deleteTilesetSources'](), undefined)
      assertStrictEquals(await d['deleteTilesetSources'](), undefined)
      assertStrictEquals(await d['deleteTilesetSources'](), undefined)
    } finally {
      aStub.restore()
      bStub.restore()
    }
  })

})
