import { assert, assertStrictEquals } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { stats } from '/src/commands/stats.ts'

Deno.test('stats', async t => {
    await t.step('test stats command', () => {
        const dataAndMappingsContentStub = stub(Deno, 'readTextFileSync', returnsNext([
            'point: [[-90,40]]',
            `- name: Point\n  type: point\n  objects: [point]`,
            'T1: [[0,0],[1,0],[1,1],[0,0]]\nT2: [[0,0],[0.2,0.4],[0.8,0.6],[1,1]]',
            `- name: Line\n  type: line\n  objects: [T1,T2]\n- name: Area\n  type: area\n  objects: [T1,T2]`,
        ]))
        try {
            assertStrictEquals(stats({} as MapperArgs), 'No line data in mappings file.\nNo area data in mappings file.')
            assert(stats({} as MapperArgs).length > 0)
        } finally {
            dataAndMappingsContentStub.restore()
        }
    })
})
