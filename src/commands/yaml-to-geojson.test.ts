import { assert } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { yamlToGeoJSON } from '/src/commands/yaml-to-geojson.ts'

Deno.test('yaml-to-geojson', async t => {
  await t.step('simple data and mappings', () => {
    const myStub = stub(Deno, 'readTextFileSync', returnsNext([
      'a: [[0,0],[1,1],[2,2]]',
      `
    - name: foo
      type: point
      objects:
        - a
      paint:
        color: red
        diameter: 1
        stroke-color: red
        stroke-width: 1
    - name: bar
      type: line
      paint:
        color: red
        width: 1
      objects:
        - a
    - name: baz
      type: area
      paint:
        fill: red
        border: red
      objects:
        - a`
    ]))

    try {
      assert(yamlToGeoJSON({} as MapperArgs).length > 0)
    } finally {
      myStub.restore()
    }
  })
})
