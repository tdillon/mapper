import { assertStrictEquals, assertThrows } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { gpxToYaml } from '/src/commands/gpx-to-yaml.ts'

Deno.test('gpx-to-yaml', async t => {

    const readDirSyncIterable: Iterable<Deno.DirEntry> = { [Symbol.iterator]: function* () { } };

    readDirSyncIterable[Symbol.iterator] = function* () {
        yield {
            isDirectory: false,
            isFile: true,
            isSymlink: false,
            name: 'bob.GpX',
        }
    }

    const validGpxToYamlTestCases = [[
        `<gpx><trk><trkseg><trkpt lat="40.1" lon="-80.1"></trkpt></trkseg></trk></gpx>`,
        'Segment 0: [[-80.1,40.1]]',
        'single segment with single point'
    ], [
        `<gpx><trk><trkseg>
            <trkpt lat="40.1" lon="-80.2"></trkpt>
            <trkpt lat="40.3" lon="-80.4"></trkpt>
        </trkseg></trk></gpx>`,
        'Segment 0: [[-80.2,40.1],[-80.4,40.3]]',
        'single segment with multiple points'
    ], [
        `<gpx><trk>
            <trkseg><trkpt lat="40.1" lon="-80.2"></trkpt></trkseg>
            <trkseg><trkpt lat="40.3" lon="-80.4"></trkpt></trkseg>
        </trk></gpx>`,
        'Segment 0: [[-80.2,40.1]]\nSegment 1: [[-80.4,40.3]]',
        'multiple segments with single point'
    ], [
        `<gpx><trk>
        <trkseg>
            <trkpt lat="40.1" lon="-80.2"></trkpt>
            <trkpt lat="40.3" lon="-80.4"></trkpt>
        </trkseg>
        <trkseg>
            <trkpt lat="40.5" lon="-80.6"></trkpt>
            <trkpt lat="40.7" lon="-80.8"></trkpt>
        </trkseg>
        </trk></gpx>`,
        'Segment 0: [[-80.2,40.1],[-80.4,40.3]]\nSegment 1: [[-80.6,40.5],[-80.8,40.7]]',
        'multiple segments with multiple points'
    ]]


    for (const tc of validGpxToYamlTestCases) {

        await t.step(tc[2], () => {
            const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
            const readTextFileSyncStub = stub(Deno, 'readTextFileSync', returnsNext(tc))
            try {
                assertStrictEquals(gpxToYaml({ root: './' } as MapperArgs), tc[1])
            } finally {
                readDirSyncStub.restore()
                readTextFileSyncStub.restore()
            }
        })
    }

    const badGpxFileExamples = [
        `<gpx><trk><trkseg><trkpt></trkpt></trkseg></trk></gpx>`,
        `<gpx><trk><trkseg></trkseg></trk></gpx>`,
        `<gpx><trk></trk></gpx>`,
        `<gpx></gpx>`,
    ]

    for (const tc of badGpxFileExamples) {
        await t.step(`bad gpx file throws: ${tc}`, () => {
            const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
            const readTextFileSyncStub = stub(Deno, 'readTextFileSync', returnsNext([tc]))
            try {
                assertThrows(() => gpxToYaml({ root: './' } as MapperArgs), Error, 'Something is not right with your GPX files.')
            } finally {
                readDirSyncStub.restore()
                readTextFileSyncStub.restore()
            }
        })
    }

    // Set readDirSynce to not return a gpx file.
    readDirSyncIterable[Symbol.iterator] = function* () {
        yield {
            isDirectory: false,
            isFile: true,
            isSymlink: false,
            name: 'foo.bar',
        }
    }

    await t.step('no gpx file throws', () => {
        const readDirSyncStub = stub(Deno, 'readDirSync', () => readDirSyncIterable)
        try {
            assertThrows(() => gpxToYaml({ root: './' } as MapperArgs), Error, 'No GPX file found.')
        } finally {
            readDirSyncStub.restore()
        }
    })
})