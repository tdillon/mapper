import { delay } from 'std/async/delay.ts'
import { bgBlue, bold, white } from 'std/fmt/colors.ts'
import { USERNAME } from '/src/utils/config.ts'
import { getDataFileContent, getGeoJSONContent, getMappingsFileContent } from '/src/utils/helpers.ts'
import { ImageSize } from '/src/utils/image-size.ts'
import { Mapbox } from '/src/utils/mapbox.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { getTilesetRequestBody, getTilesetSourceID } from '/src/utils/template-recipe.ts'
import { getStyleTemplate } from '/src/utils/template-style.ts'
import { BoundingBox, DataYaml, GeoJSON, JobStatus, MapboxOwnedStyle, MappingsThing } from '/src/utils/types.ts'

export default class {

    private dataFileContent: DataYaml
    private mappingsFileContent: Array<MappingsThing>
    private geoJSONFullPath: string
    private ndjsonFileContent: string
    private ndjsonFullPath: string
    private ndjsonFilename: string
    private tilesetRequestBody
    private geoJSON: GeoJSON

    constructor(private args2: MapperArgs) {
        this.geoJSONFullPath = `${this.args2.root}/${this.args2.prefix}.geojson`
        this.ndjsonFileContent = ''
        this.ndjsonFullPath = `${this.args2.root}/${this.args2.prefix}.ndjson`
        this.ndjsonFilename = `${this.args2.prefix}.ndjson`
        this.tilesetRequestBody = getTilesetRequestBody(this.args2.prefix, USERNAME)
        this.dataFileContent = getDataFileContent(args2.dataFileFullPath)
        this.mappingsFileContent = getMappingsFileContent(args2.mappingsFileFullPath, this.dataFileContent)
        this.geoJSON = getGeoJSONContent(this.dataFileContent, this.mappingsFileContent)
    }

    private styleFileFullPath(styleId: string) {
        return `${this.args2.root}/${this.args2.prefix}-${styleId}-style.json`
    }

    private printInfo() {
        console.log(bold(white(bgBlue('\nCommandline args:'))))
        console.log(this.args2)

        console.log(bold(white(bgBlue('\nRuntime properties:'))))
        console.log(`${bold('Base Directory')}: ${this.args2.root}`)
        console.log(`${bold('Data File')}: ${this.args2.dataFileFullPath}`)
        console.log(`${bold('Mappings File')}: ${this.args2.mappingsFileFullPath}`)
        console.log(`${bold('Output Files Prefix')}: ${this.args2.prefix}`)
        console.log(`${bold('.geojson Full Path')}: ${this.geoJSONFullPath}`)
        console.log(`${bold('.ndjson Full Path')}: ${this.ndjsonFullPath}`)
        console.log(`${bold('.ndjson Filename')}: ${this.ndjsonFilename}`)
        for (const basemap of this.args2.basemaps) {
            console.log(`${bold('Mapbox Basemap')}: ${basemap}`)
        }
        for (const basemap of this.args2.basemaps) {
            console.log(`${bold('Style File')}: ${this.styleFileFullPath(basemap)}`)
        }
        for (const size of this.args2.sizes) {
            console.log(`${bold('Image Size')}: ${size.toString()}`)
        }
        for (const basemap of this.args2.basemaps) {
            for (const size of this.args2.sizes) {
                console.log(`${bold('Image File')}: ${this.args2.root}/${this.args2.prefix}-${basemap}-${size.toString()}.png`)
            }
        }
    }

    private async deleteFile(fullPath: string) {
        console.log(`\nRequest to delete ${bold(fullPath)}`)

        try {
            await Deno.remove(fullPath);
            console.log(`Deleted.`)
        } catch (error) {
            if (error instanceof Deno.errors.NotFound) {
                console.log(`Does not exist, no action taken.`)
            } else {
                throw error;
            }
        }
    }

    private async deleteStyles() {
        console.log(`\nDeleting Mapbox styles for current run.`)
        const existingStyles = await Mapbox.getAllStyles()
        if (existingStyles.length) {
            console.log('Existing Styles:')
            existingStyles.forEach(s => console.log(`    - ${s.name}`))

            for (const style of existingStyles.filter(s => s.name.includes(` - ${this.args2.prefix} Style`))) {
                console.log(`Deleting ${style.name}`)
                await Mapbox.deleteStyle(style.id)
            }
        } else {
            console.log('No pertinent styles to delete, no action taken.')
        }
    }

    private async deleteTilesets() {
        console.log(`\nDeleting Mapbox tilesets for current run.`)
        const existingTilesets = await Mapbox.getAllTilesets()
        if (existingTilesets.length) {
            console.log('Existing Tilesets:');
            existingTilesets.forEach(t => console.log(`    - ${t}`))
            const tilesetName = `${USERNAME}.${this.args2.prefix}`
            if (existingTilesets.find(t => t === tilesetName)) {
                console.log(`Deleting ${tilesetName}`)
                await Mapbox.deleteTileset(this.args2.prefix)
            } else {
                console.log(`No pertinent tileset to delete, no action taken.`)
            }
        } else {
            console.log(`No pertinent tileset to delete, no action taken.`)
        }
    }

    private async deleteTilesetSources() {
        console.log(`\nDeleting Mapbox tileset sources for current run.`)
        const existingTilesetSources = await Mapbox.getAllTilesetSources()
        if (existingTilesetSources.length) {
            console.log('Existing Tileset Sources:');
            existingTilesetSources.forEach(t => console.log(`    - ${t}`))
            const tilesetSourceName = getTilesetSourceID(this.args2.prefix, USERNAME)
            if (existingTilesetSources.find(t => t === tilesetSourceName)) {
                console.log(`Deleting ${tilesetSourceName}`)
                await Mapbox.deleteTilesetSource(this.args2.prefix)
            } else {
                console.log(`No pertinent tileset source to delete, no action taken.`)
            }
        } else {
            console.log(`No pertinent tileset to delete, no action taken.`)
        }
    }

    private createNDJsonFile() {
        console.log(`\nCreating ${bold(this.ndjsonFullPath)}`)
        this.ndjsonFileContent = ''
        this.geoJSON.features.forEach(f => this.ndjsonFileContent += `${JSON.stringify(f)}\n`)
        Deno.writeTextFileSync(this.ndjsonFullPath, this.ndjsonFileContent)
    }

    private createGeoJSONFile() {
        console.log(`\nCreating ${bold(this.geoJSONFullPath)}`)
        Deno.writeTextFileSync(this.geoJSONFullPath, JSON.stringify(this.geoJSON));
    }

    private async createStyleFile(styleId: MapboxOwnedStyle) {
        console.log(`\nCreating ${bold(this.styleFileFullPath(styleId))}`)

        const resData = await Mapbox.getStyle(styleId, 'mapbox')

        resData.name = `${styleId} - ${this.args2.prefix} Style`

        delete resData.bearing;
        delete resData.created;
        delete resData.draft;
        delete resData.id;
        delete resData.modified;
        delete resData.owner;
        delete resData.pitch;

        if (styleId === 'satellite-v9') {
            resData.sources.composite = {
                url: `mapbox://${USERNAME}.${this.args2.prefix}`,
                type: 'vector'
            }
        } else {
            resData.sources.composite.url += `,${USERNAME}.${this.args2.prefix}`
        }

        // On outdoor style, update contour lines from metric to American.
        const contourLabel = resData.layers.find(l => l.id === 'contour-label')
        if (contourLabel) {
            contourLabel.layout["text-field"] = ["to-string", ["concat", ["round", ["*", ["get", "ele"], 3.28084]], " ft"]]
        }

        resData.layers.push(...this.mappingsFileContent.map(a => getStyleTemplate(a)))

        Deno.writeTextFileSync(this.styleFileFullPath(styleId), JSON.stringify(resData));
    }

    private async createTilesetSource() {
        console.log('\nCreating Tileset Source')

        const form = new FormData();
        const file = new File([this.ndjsonFileContent], this.ndjsonFilename);
        form.append('file', file, this.ndjsonFilename)

        await Mapbox.createTilesetSource(this.args2.prefix, form)
    }

    private async createTileset() {
        console.log('\nCreating Tileset')
        await Mapbox.createTileset(this.args2.prefix, JSON.stringify(this.tilesetRequestBody))
    }

    private async publishTileset() {
        console.log('\nPublishing Tileset')
        const jobId = await Mapbox.publishTileset(this.args2.prefix)
        console.log(`Job created: ${bold(jobId)}`)
        return jobId
    }

    private async waitForJobToFinish(jobId: string, delaySeconds = 20) {
        console.log(`\nWait for job ${bold(jobId)} to finish`);

        let jobStatus: JobStatus;

        do {
            console.log(`Waiting ${delaySeconds} seconds to check for completed job.`)
            await delay(delaySeconds * 1000)
            jobStatus = await Mapbox.getJobStatus(jobId, this.args2.prefix)
        } while (jobStatus === "processing" || jobStatus === "queued");
    }

    private async createStyle(style: MapboxOwnedStyle) {
        console.log(`\nCreating style ${bold(style)}`)

        const styleId = await Mapbox.createStyle(await Deno.readTextFile(this.styleFileFullPath(style)))

        console.log(`Style created: ${bold(styleId)}`)

        return styleId;
    }

    private getBoundingBox() {
        const allLatLons = this.geoJSON.features.map(f => {
            if (f.geometry.type === "MultiPoint") {
                return f.geometry.coordinates
            } else if (f.geometry.type === 'LineString') {
                return f.geometry.coordinates
            } else if (f.geometry.type === "Polygon") {
                return f.geometry.coordinates.flat(1)
            } else {
                throw Error('how?')
            }
        }).flat()

        const boundingBox = <BoundingBox>[Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER]

        allLatLons.forEach(latlon => {
            if (latlon[0] < boundingBox[0]) {
                boundingBox[0] = latlon[0]
            }
            if (latlon[0] > boundingBox[2]) {
                boundingBox[2] = latlon[0]
            }
            if (latlon[1] < boundingBox[1]) {
                boundingBox[1] = latlon[1]
            }
            if (latlon[1] > boundingBox[3]) {
                boundingBox[3] = latlon[1]
            }
        })

        return boundingBox;
    }

    /** {lon},{lat} */
    private getCenter(): string {
        const b = this.getBoundingBox();
        return `${((b[2] - b[0]) / 2) + b[0]},${((b[3] - b[1]) / 2) + b[1]}`
    }

    private async getImage(styleId: string, style: MapboxOwnedStyle, size: ImageSize) {
        console.log(`\nDownloading image: ${bold(styleId)} - ${bold(style)} - ${size.toString()}${size.twoX ? ' - @2x' : ''}`)

        const buf = await Mapbox.getImage(styleId, size, size.isZoom ? this.getCenter() : JSON.stringify(this.getBoundingBox()), size.isBoundingBox ? this.args2.padding : undefined)
        const imageFullPath = `${this.args2.root}/${this.args2.prefix}-${style}-${size.toString()}.png`

        await Deno.writeFile(imageFullPath, buf);
        console.log(`Downloaded: ${bold(imageFullPath)}`)
    }

    private async deleteStyle(styleId: string) {
        console.log(`\nDeleting style ${bold(styleId)}`)
        await Mapbox.deleteStyle(styleId)
    }

    private async deleteTileset() {
        console.log(`\nDeleting tileset ${bold(this.args2.prefix)}`)
        await Mapbox.deleteTileset(this.args2.prefix)
    }

    private async deleteTilesetSource() {
        console.log(`\nDeleting tileset source ${bold(this.args2.prefix)}`)
        await Mapbox.deleteTilesetSource(this.args2.prefix)
    }

    async doMapboxStuff() {
        this.printInfo()

        console.log(bold(white(bgBlue('\nFILESYSTEM CLEANUP'))))
        await this.deleteFile(this.ndjsonFullPath)
        await this.deleteFile(this.geoJSONFullPath)
        for (const styleId of this.args2.basemaps) {
            await this.deleteFile(this.styleFileFullPath(styleId))
        }

        console.log(bold(white(bgBlue('\nMAPBOX CLEANUP'))))
        await this.deleteStyles()
        await this.deleteTilesets()
        await this.deleteTilesetSources()

        console.log(bold(white(bgBlue('\nFILESYSTEM CREATION'))))
        this.createNDJsonFile()
        this.createGeoJSONFile()
        for (const basemap of this.args2.basemaps) {
            await this.createStyleFile(basemap)
        }

        console.log(bold(white(bgBlue('\nMAPBOX PROCESSING'))))
        await this.createTilesetSource()
        await this.createTileset()
        const jobId = await this.publishTileset()
        await this.waitForJobToFinish(jobId)
        for (const basemap of this.args2.basemaps) {
            const styleId = await this.createStyle(basemap)
            for (const size of this.args2.sizes) {
                await delay(5000)
                await this.getImage(styleId, basemap, size)
            }
            await this.deleteStyle(styleId)
        }

        console.log(bold(white(bgBlue('\nMAPBOX CLEANUP'))))
        await this.deleteTileset()
        await this.deleteTilesetSource()

        console.log(bold(white(bgBlue('\nFILESYSTEM CLEANUP'))))
        await this.deleteFile(this.ndjsonFullPath)
        for (const basemap of this.args2.basemaps) {
            await this.deleteFile(this.styleFileFullPath(basemap))
        }
    }
}
