import { assert } from 'std/assert/mod.ts'
import { HELP } from '/src/commands/help.ts'

Deno.test('the help message has some text', () => {
    const minHelpLength = 7600
    assert(HELP.length > minHelpLength, `Help text contains at least ${minHelpLength} characters.`)
})