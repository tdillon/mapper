import { default as CheapRuler } from 'CheapRuler'
import { sprintf } from 'std/fmt/printf.ts'
import { getDataFileContent, getMappingsFileContent } from '/src/utils/helpers.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import { DataYaml, MappingsThing } from '/src/utils/types.ts'

/** An individual item (i.e., a single line or area) to print the stats for. */
interface StatItem {
    /** The name of an individual item. */
    name: string;
    /** The measurement of the item.  The unit of measurement is `ft` for lines, and `ft²` for areas. */
    measure: number;
    /** The number of lon/lat coordinates in this item. */
    numPoints: number;
}

/** Print a stat table. */
function printGenericStatTable(sectionName: string, type: 'line' | 'area', items: Array<StatItem>): string {
    const unit = type === 'line' ? 'ft' : 'ft²'
    /** base unit decimals - the number of decimal places used for the default measure */
    const bud = type === 'line' ? 0 : 1
    const maxItemNameLength = items.reduce((p, c) => Math.max(p, c.name.length), 0)
    const maxNumCoordLength = items.reduce((p, c) => Math.max(p, c.numPoints), 0).toString().length
    items.sort((a, b) => b.measure - a.measure)  // sort by measure descending

    const conversions: Array<{
        unit: string,
            /** conversion factor - divides the default measure to convert into a different unit */ cf: number,
            /** the numbe rof decimal places used for this unit of measure */ decimals: number
    }> = (type === 'line' ? [
        { unit: 'yd', cf: 3, decimals: 1 },
        { unit: 'mi', cf: 5280, decimals: 2 }
    ] : [
        { unit: 'ac', cf: 43_560, decimals: 2 }
    ])

    // summary stats
    const numCoords = items.reduce((p, c) => p + c.numPoints, 0)
    const total = items.reduce((p, c) => p + c.measure, 0)
    const average = total / items.length
    const max = items.reduce((p, c) => Math.max(p, c.measure), 0)
    const min = items.reduce((p, c) => Math.min(p, c.measure), Number.MAX_SAFE_INTEGER)

    /**
     * width of table (not including box characters)
     *
     * I'm assuming (until a counter example is found) the **ITEMS** section is wider than the **SUMMARY** section.
     */
    const tableWidth =
        6 + // indention
        maxItemNameLength + // item names
        3 + // space colon space
        max.toFixed(bud).length + // default measure
        1 + // space
        unit.length + // default unit of measure
        3 + // space hyphen space
        conversions.reduce((p, c) => p + (max / c.cf).toFixed(c.decimals).length + 1 + c.unit.length, 0) + // conversions
        3 * (conversions.length - 1) + // conversion separators
        3 + // space hyphen space
        maxNumCoordLength + // # coords
        1 + // space
        6 + // coords
        6 // padding


    /**
     * Get a string of all measures for a given number `n`.
     * The measures are padding according to a large number `biggest`.
     *
     * @example
     * // 482 ft - 160.7 yd - 0.09 mi
     *
     * @example
     * // this examples shows padding when `biggest` is larger than `n`
     * //   482 ft -   160.7 yd -  0.09 mi
     *
     * @example
     * // 10029.0 ft² - 0.23 ac
     *
     * @param biggest - the largest number which determines column width (most likely `total` or `max`)
     * @param n - the base number to print in addition to all conversions of it
     * @returns see examples for output
     */
    const getMeasuresString = (biggest: number, n: number) => sprintf('%*s %s - %s',
        biggest.toFixed(bud).length,
        n.toFixed(bud),
        unit,
        conversions.map(i => sprintf('%*s %s',
            (biggest / i.cf).toFixed(i.decimals).length,
            (n / i.cf).toFixed(i.decimals),
            i.unit)).join(' - '))

    return `
┌${'─'.repeat(tableWidth)}┐
│${' '.repeat(tableWidth)}│
${sectionName.split(new RegExp(`(.{${tableWidth - 4}})`, 'g')).filter(s => s !== '').map(s => sprintf('│  %-*s  │', tableWidth - 4, s)).join('\n')}
│${' '.repeat(tableWidth)}│
├─── SUMMARY ${'─'.repeat(tableWidth - 12)}┤
│${' '.repeat(tableWidth)}│
│      ${type} count  : ${sprintf('%-*d', tableWidth - 20, items.length)}│
│      # of coords : ${sprintf('%-*d', tableWidth - 20, numCoords)}│
│            total : ${sprintf('%-*s', tableWidth - 20, getMeasuresString(total, total))}│
│              max : ${sprintf('%-*s', tableWidth - 20, getMeasuresString(total, max))}│
│          average : ${sprintf('%-*s', tableWidth - 20, getMeasuresString(total, average))}│
│              min : ${sprintf('%-*s', tableWidth - 20, getMeasuresString(total, min))}│
│${' '.repeat(tableWidth)}│
├─── ITEMS ${'─'.repeat(tableWidth - 10)}┤
│${' '.repeat(tableWidth)}│
${items.map(i => sprintf(`│      %*-s : %s - %*d coords      │`,
        maxItemNameLength,
        i.name,
        getMeasuresString(max, i.measure),
        maxNumCoordLength,
        i.numPoints)).join('\n')
        }
│${' '.repeat(tableWidth)}│
└${'─'.repeat(tableWidth)}┘
`
}

/** Print all stats for the given `type`. */
function printGenericStats(type: 'line' | 'area', dataFileContent: DataYaml, mappingsFileContent: Array<MappingsThing>): string {
    const data: Array<string> = []

    // I'm assuming data is all contained in some 'small' area.
    const ruler = new CheapRuler(Object.values(dataFileContent)[0][0][1], 'feet')

    const uniqueData = Array.from(
        new Set(mappingsFileContent
            .filter(m => m.type === type)
            .flatMap(ml => ml.objects)
        )
    ).map<StatItem>(name => ({
        name,
        measure: type === 'line' ? ruler.lineDistance(dataFileContent[name]) : ruler.area([dataFileContent[name]]),
        numPoints: dataFileContent[name].length
    }));

    if (uniqueData.length > 0) {
        data.push(printGenericStatTable(`All ${type.toUpperCase()} Objects`, type, uniqueData))
        mappingsFileContent
            .filter(m => m.type === type)
            .forEach(m => data.push(printGenericStatTable(m.name, type, uniqueData.filter(d => m.objects.includes(d.name)))))
    } else {
        data.push(`No ${type} data in mappings file.`)
    }

    return data.join('\n')
}

/** Print stats for all lines and areas configured in the mappings file. */
export function stats(args: MapperArgs) {
    const dataFileContent: DataYaml = getDataFileContent(args.dataFileFullPath)
    const mappingsFileContent: Array<MappingsThing> = getMappingsFileContent(args.mappingsFileFullPath, dataFileContent)

    return [
        printGenericStats('line', dataFileContent, mappingsFileContent),
        printGenericStats('area', dataFileContent, mappingsFileContent)
    ].join('\n')
}
