import { assert, assertRejects } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { getUpgradeText } from '/src/commands/upgrade.ts'
import { VERSION } from '/src/commands/version.ts'

Deno.test('upgrade text', async () => {
    const myStub = stub(window, 'fetch', returnsNext([
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res({
                data: {
                    project: {
                        releases: {
                            nodes: [{
                                name: 'v0.0.0',
                                description: 'foo'
                            }]
                        }
                    }
                }
            }))
        } as Response)),
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res({
                data: {
                    project: {
                        releases: {
                            nodes: [{
                                name: `v${VERSION}`,
                                description: 'foo'
                            }]
                        }
                    }
                }
            }))
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('foo'))
        } as Response))
    ]))

    try {
        assert(await getUpgradeText())
        assert(await getUpgradeText())
        assertRejects(async () => await getUpgradeText())
    } finally {
        myStub.restore()
    }
})
