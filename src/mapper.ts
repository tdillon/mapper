import { parseArgs } from 'std/cli/parse_args.ts'
import { geoJSONToYaml } from '/src/commands/geojson-to-yaml.ts'
import { gpxToYaml } from '/src/commands/gpx-to-yaml.ts'
import { HELP } from '/src/commands/help.ts'
import { simplify } from '/src/commands/simplify.ts'
import { stats } from '/src/commands/stats.ts'
import { getUpgradeText } from '/src/commands/upgrade.ts'
import { VERSION } from '/src/commands/version.ts'
import { yamlToGeoJSON } from '/src/commands/yaml-to-geojson.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'
import Default from '/src/commands/default.ts'

const a = new MapperArgs(parseArgs(Deno.args, { collect: ['basemap', 'size', 'ratio'] }))

switch (a.MODE) {
    case 'HELP':
        console.log(HELP)
        break
    case 'VERSION':
        console.log(`mapper ${VERSION}`)
        break
    case 'UPGRADE':
        console.log(await getUpgradeText())
        break
    case 'STATS':
        console.log(stats(a))
        break
    case 'SIMPLIFY':
        console.log(simplify(a))
        break
    case 'GEOJSON-TO-YAML':
        console.log(geoJSONToYaml(a))
        break
    case 'GPX-TO-YAML':
        console.log(gpxToYaml(a))
        break
    case 'YAML-TO-GEOJSON':
        console.log(yamlToGeoJSON(a))
        break
    case 'DEFAULT':
        await new Default(a).doMapboxStuff()
        break
    default: throw new Error('CLI mode not known!')
}
