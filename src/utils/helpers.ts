import { parse } from 'std/yaml/parse.ts'
import { DataYaml, Feature, GeoJSON, MappingsThing } from '/src/utils/types.ts'

/** Validate data file object. */
export function _validateDataFileContent(dataFileContent: DataYaml) {
    if (!dataFileContent) {
        throw new Error(`Invalid data file.  The file must have content: ${JSON.stringify(dataFileContent)}`)
    }

    for (const propName in dataFileContent) {
        const lonLatArray = dataFileContent[propName]

        if (!lonLatArray || typeof lonLatArray !== 'object' || !('length' in lonLatArray) || lonLatArray.length === 0) {
            throw new Error(`Invalid data file.  ${propName} does not have an array of longitude/latitudes: ${JSON.stringify(lonLatArray)}`)
        }

        for (const lonlat of lonLatArray) {
            if (!(lonlat.length === 2 &&
                typeof lonlat[0] === 'number' &&
                lonlat[0] >= -180 &&
                lonlat[0] <= 180 &&
                typeof lonlat[1] === 'number' &&
                lonlat[1] >= -90 &&
                lonlat[1] <= 90)) {
                throw new Error(`Invalid data file.  ${propName} has bad data: ${JSON.stringify(lonlat)}`)
            }
        }
    }
}

/** Validate mappings file object. */
export function _validateMappingsFileContent(mappingsFileContent: Array<MappingsThing>, dataFileContent: DataYaml) {
    if (!mappingsFileContent || typeof mappingsFileContent !== 'object' || !('length' in mappingsFileContent) || mappingsFileContent.length === 0) {
        throw new Error(`Invalid mappings file.  The file does not have an array of proper objects: ${JSON.stringify(mappingsFileContent)}`)
    }

    if (new Set(mappingsFileContent.map(m => m.name)).size !== mappingsFileContent.length) {
        throw new Error(`Invalid mappings file.  Mapping object names must be unique: ${JSON.stringify(mappingsFileContent)}`)
    }

    for (const m of mappingsFileContent) {
        if (!m.name || typeof m.name !== 'string') {
            throw new Error(`Invalid mappings file.  Each object must have name property which is a string: ${JSON.stringify(m)}`)
        }

        if (!m.type || !(m.type === 'line' || m.type === 'point' || m.type === 'area')) {
            throw new Error(`Invalid mappings file.  Each object must have type property which is (line | point | area): ${JSON.stringify(m)}`)
        }

        if (!m.objects || typeof m.objects !== 'object' || !('length' in m.objects) || m.objects.length === 0) {
            throw new Error(`Invalid mappings file.  Each object must have objects property which is an array of strings: ${JSON.stringify(m)}`)
        }

        if (m.objects.find(i => !(i in dataFileContent))) {
            throw new Error(`Invalid mappings file.  Each object's objects array must include items from data yaml file: ${JSON.stringify(m)}`)
        }

        if (!Object.keys(m).every(p => ['name', 'type', 'objects', 'paint'].includes(p))) {
            throw new Error(`Invalid mappings file.  Each object must have (name, type, objects) and optionally (paint) properties: ${JSON.stringify(m)}`)
        }

        if (typeof m.paint !== 'undefined' && typeof m.paint !== 'object') {
            throw new Error(`Invalid mappings file.  paint must be an object: ${JSON.stringify(m)}`)
        }

        if (m.paint) {
            if (m.type === 'area' &&
                (!Object.keys(m.paint).every(k => ['fill', 'border'].includes(k)) ||
                    (typeof m.paint.border !== 'undefined' && typeof m.paint.border !== 'string') ||
                    (typeof m.paint.fill !== 'undefined' && typeof m.paint.fill !== 'string'))) {
                throw new Error(`Invalid mappings file.  Paint for areas can optionally have fill and border: ${JSON.stringify(m)}`)
            } else if (m.type === 'line' &&
                (!Object.keys(m.paint).every(k => ['color', 'width'].includes(k)) ||
                    (typeof m.paint.color !== 'undefined' && typeof m.paint.color !== 'string') ||
                    (typeof m.paint.width !== 'undefined' && typeof m.paint.width !== 'number'))) {
                throw new Error(`Invalid mappings file.  Paint for lines can optionally have color and width: ${JSON.stringify(m)}`)
            } else if (m.type === 'point' &&
                (!Object.keys(m.paint).every(k => ['color', 'diameter', 'stroke-color', 'stroke-width'].includes(k)) ||
                    (typeof m.paint.color !== 'string') ||
                    (typeof m.paint.diameter !== 'number') ||
                    (typeof m.paint['stroke-color'] !== 'string') ||
                    (typeof m.paint['stroke-width'] !== 'number'))) {
                throw new Error(`Invalid mappings file.  Paint for points can optionally have color, diameter, stroke-color, and stroke-width: ${JSON.stringify(m)}`)
            }
        }
    }
}

/** Returns data file object after validating */
export function getDataFileContent(dataFileFullPath: string): DataYaml {
    const dataFileContent = <DataYaml>parse(Deno.readTextFileSync(dataFileFullPath))
    _validateDataFileContent(dataFileContent)
    return dataFileContent
}

/** Returns mappings file object after validating */
export function getMappingsFileContent(mappingsFileFullPath: string, dataFileContent: DataYaml): Array<MappingsThing> {
    const mappingsFileContent = <Array<MappingsThing>>parse(Deno.readTextFileSync(mappingsFileFullPath))
    _validateMappingsFileContent(mappingsFileContent, dataFileContent)
    return mappingsFileContent
}

/** Returns features for GeoJSON file */
export function _getFeatures(dataFileContent: DataYaml, mappingsFileContent: Array<MappingsThing>): Array<Feature> {
    return mappingsFileContent.flatMap(mapping => mapping.objects.map(objName => {
        return <Feature>{
            type: 'Feature',
            properties: {
                name: `${mapping.name} - ${objName}`,
                dataName: objName,
                mappingName: mapping.name,
                ...{  // Display styles for geojson.io.  Make sure Mapbox doesn't care.
                    point: {
                        'marker-color': mapping.type === 'point' ? mapping.paint?.color : '',
                        'marker-size': 'small'
                    },
                    line: {
                        stroke: mapping.type === 'line' ? mapping.paint?.color : ''
                    },
                    area: {
                        stroke: mapping.type === 'area' ? mapping.paint?.border : '',
                        fill: mapping.type === 'area' ? mapping.paint?.fill : ''
                    }
                }[mapping.type]
            },
            geometry: {
                type: ({ area: 'Polygon', line: 'LineString', point: 'MultiPoint' })[mapping.type],
                coordinates: mapping.type === 'area'
                    ? [[...dataFileContent[objName], dataFileContent[objName][0]]]
                    : dataFileContent[objName]
            }
        }
    }))
}

/** Returns GeoJSON file object */
export function getGeoJSONContent(dataFileContent: DataYaml, mappingsFileContent: Array<MappingsThing>): GeoJSON {
    return { type: 'FeatureCollection', features: _getFeatures(dataFileContent, mappingsFileContent) }
}
