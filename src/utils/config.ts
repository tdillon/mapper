export const USERNAME = Deno.env.get('MAPBOX_USERNAME') || ''
export const TOKEN = Deno.env.get('MAPBOX_TOKEN') || ''