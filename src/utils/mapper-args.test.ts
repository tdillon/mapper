import { assertEquals, assertStrictEquals, assertThrows } from 'std/assert/mod.ts'
import { parseArgs } from 'std/cli/parse_args.ts'
import { ImageSize } from '/src/utils/image-size.ts'
import { MapperArgs } from '/src/utils/mapper-args.ts'

let a: MapperArgs

function parseFlags(args: Array<string>) {
    return parseArgs(args, { collect: ['basemap', 'size', 'ratio'] })
}

Deno.test('valid arguments for MapperArgs', async (t) => {

    await t.step('version command', () => {
        a = new MapperArgs(parseFlags(['version']))
        assertStrictEquals(a.MODE, 'VERSION')
    })

    await t.step('help command', () => {
        a = new MapperArgs(parseFlags(['help']))
        assertStrictEquals(a.MODE, 'HELP')
    })

    await t.step('upgrade command', () => {
        a = new MapperArgs(parseFlags(['upgrade']))
        assertStrictEquals(a.MODE, 'UPGRADE')
    })

    await t.step('gpx-to-yaml command', async (t) => {
        await t.step('using defaults', () => {
            a = new MapperArgs(parseFlags(['gpx-to-yaml']))
            assertStrictEquals(a.MODE, 'GPX-TO-YAML')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
        })

        await t.step('root w/ trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['gpx-to-yaml', '--root', `${root}/`]))
            assertStrictEquals(a.MODE, 'GPX-TO-YAML')
            assertStrictEquals(a.root, root)
        })

        await t.step('root w/o trailing /', () => {
            const root = '/foo/bar'
            a = new MapperArgs(parseFlags(['gpx-to-yaml', '--root', root]))
            assertStrictEquals(a.MODE, 'GPX-TO-YAML')
            assertStrictEquals(a.root, root)
        })
    })

    await t.step('yaml-to-geojson command', async (t) => {
        await t.step('using defaults', () => {
            a = new MapperArgs(parseFlags(['yaml-to-geojson']))
            assertStrictEquals(a.MODE, 'YAML-TO-GEOJSON')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
        })

        await t.step('root w/ trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['yaml-to-geojson', '--root', `${root}/`]))
            assertStrictEquals(a.MODE, 'YAML-TO-GEOJSON')
            assertStrictEquals(a.root, root)
        })

        await t.step('root w/o trailing /', () => {
            const root = '/foo/bar'
            a = new MapperArgs(parseFlags(['yaml-to-geojson', '--root', root]))
            assertStrictEquals(a.MODE, 'YAML-TO-GEOJSON')
            assertStrictEquals(a.root, root)
        })
    })

    await t.step('geojson-to-yaml command', async (t) => {
        await t.step('using defaults', () => {
            a = new MapperArgs(parseFlags(['geojson-to-yaml']))
            assertStrictEquals(a.MODE, 'GEOJSON-TO-YAML')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
        })

        await t.step('root w/ trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['geojson-to-yaml', '--root', `${root}/`]))
            assertStrictEquals(a.MODE, 'GEOJSON-TO-YAML')
            assertStrictEquals(a.root, root)
        })

        await t.step('root w/o trailing /', () => {
            const root = '/foo/bar'
            a = new MapperArgs(parseFlags(['geojson-to-yaml', '--root', root]))
            assertStrictEquals(a.MODE, 'GEOJSON-TO-YAML')
            assertStrictEquals(a.root, root)
        })
    })

    await t.step('stats command', async (t) => {

        await t.step('using defaults', () => {
            a = new MapperArgs(parseFlags(['stats']))
            assertStrictEquals(a.MODE, 'STATS')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
        })

        await t.step('root w/ trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['stats', '--root', `${root}/`]))
            assertStrictEquals(a.MODE, 'STATS')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
        })

        await t.step('root w/o trailing /', () => {
            const root = '/foo/bar'
            a = new MapperArgs(parseFlags(['stats', '--root', root]))
            assertStrictEquals(a.MODE, 'STATS')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
        })

        await t.step('data', () => {
            const dataFile = '/foo/bar.yaml'
            a = new MapperArgs(parseFlags(['stats', '--data', dataFile]))
            assertStrictEquals(a.MODE, 'STATS')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, dataFile)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
        })

        await t.step('mappings', () => {
            const mappingsFile = '/foo/bar.yaml'
            a = new MapperArgs(parseFlags(['stats', '--mappings', mappingsFile]))
            assertStrictEquals(a.MODE, 'STATS')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, mappingsFile)
        })

        await t.step('all arguments', () => {
            a = new MapperArgs(parseFlags(['stats', '--root=../foo/bar/', '--data=/tmp/my.yaml', '--mappings=a/folder/b.yaml']))
            assertStrictEquals(a.MODE, 'STATS')
            assertStrictEquals(a.root, '../foo/bar')
            assertStrictEquals(a.dataFileFullPath, '/tmp/my.yaml')
            assertStrictEquals(a.mappingsFileFullPath, 'a/folder/b.yaml')
        })

    })

    await t.step('simplify command', async (t) => {

        await t.step('using defaults', () => {
            a = new MapperArgs(parseFlags(['simplify']))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertStrictEquals(a.offset, MapperArgs.DEFAULT_OFFSET)
        })

        await t.step('root w/ trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['simplify', '--root', `${root}/`]))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertStrictEquals(a.offset, MapperArgs.DEFAULT_OFFSET)
        })

        await t.step('root w/o trailing /', () => {
            const root = '/foo/bar'
            a = new MapperArgs(parseFlags(['simplify', '--root', root]))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertStrictEquals(a.offset, MapperArgs.DEFAULT_OFFSET)
        })

        await t.step('data', () => {
            const dataFile = '/foo/bar.yaml'
            a = new MapperArgs(parseFlags(['simplify', '--data', dataFile]))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, dataFile)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertStrictEquals(a.offset, MapperArgs.DEFAULT_OFFSET)
        })

        await t.step('mappings', () => {
            const mappingsFile = '/foo/bar.yaml'
            a = new MapperArgs(parseFlags(['simplify', '--mappings', mappingsFile]))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, mappingsFile)
            assertStrictEquals(a.offset, MapperArgs.DEFAULT_OFFSET)
        })

        await t.step('multiple offset uses last', () => {
            const offset = 3.14
            a = new MapperArgs(parseFlags(['simplify', '--offset=1.1', '--offset', offset.toString()]))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertStrictEquals(a.offset, offset)
        })

        await t.step('all arguments', () => {
            a = new MapperArgs(parseFlags(['simplify', '--root=../foo/bar/', '--data=/tmp/my.yaml', '--mappings=a/folder/b.yaml', '--offset=42']))
            assertStrictEquals(a.MODE, 'SIMPLIFY')
            assertStrictEquals(a.root, '../foo/bar')
            assertStrictEquals(a.dataFileFullPath, '/tmp/my.yaml')
            assertStrictEquals(a.mappingsFileFullPath, 'a/folder/b.yaml')
            assertStrictEquals(a.offset, 42)
        })

    })

    await t.step('default mode', async (t) => {

        await t.step('using defaults', () => {
            a = new MapperArgs(parseFlags([]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('root w/ trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['--root', `${root}/`]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('root w/o trailing /', () => {
            const root = '/foo/bar'  // internal representation won't have trailing /
            a = new MapperArgs(parseFlags(['--root', root]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })


        await t.step('multiple root uses last', () => {
            const root = '/bat'
            a = new MapperArgs(parseFlags(['--root', '/foo/bar', `--root=${root}`]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, root)
            assertStrictEquals(a.dataFileFullPath, `${root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('multiple data uses last', () => {
            const dataFile = '/tmp/my.yaml'
            a = new MapperArgs(parseFlags(['--data=/foo/bar.yaml', '--data', dataFile]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, dataFile)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('multiple mappings uses last', () => {
            const mappingsFile = '/tmp/my.yaml'
            a = new MapperArgs(parseFlags(['--mappings=foo', '--mappings', mappingsFile]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, mappingsFile)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('basemap', () => {
            a = new MapperArgs(parseFlags(['--basemap', 'light', '--basemap=outdoors', '--basemap', 'satellite', '--basemap=dark']))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps.sort(), ['outdoors-v12', 'dark-v11', 'light-v11', 'satellite-v9'].sort())
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('basemap w/ duplicates', () => {
            a = new MapperArgs(parseFlags(['--basemap=outdoors', '--basemap', 'light', '--basemap=light', '--basemap=outdoors', '--basemap=light']))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps.sort(), ['light-v11', 'outdoors-v12'].sort())
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('size', () => {
            a = new MapperArgs(parseFlags(['--size', '200x200', '--size=200x200x15.5']))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [ImageSize.fromSize(200, 200), ImageSize.fromSize(200, 200, 15.5)])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('ratio', () => {
            a = new MapperArgs(parseFlags(['--ratio', '1x1', '--ratio=1x1x15.5']))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [ImageSize.fromRatio(1, 1), ImageSize.fromRatio(1, 1, 15.5)])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('multiple padding uses last', () => {
            a = new MapperArgs(parseFlags(['--padding=2', '--padding', '25']))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, 25)
            // not testing default prefix since it depends on filesystem
        })

        await t.step('multiple prefix uses last', () => {
            a = new MapperArgs(parseFlags(['--prefix=foo', '--prefix', 'foo-bar-baz']))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, MapperArgs.DEFAULT_ROOT)
            assertStrictEquals(a.dataFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`)
            assertStrictEquals(a.mappingsFileFullPath, `${MapperArgs.DEFAULT_ROOT}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`)
            assertEquals(a.basemaps, MapperArgs.DEFAULT_BASEMAP)
            assertEquals(a.sizes, [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO])
            assertStrictEquals(a.padding, MapperArgs.DEFAULT_PADDING)
            assertStrictEquals(a.prefix, 'foo-bar-baz')
        })

        await t.step('all arguments', () => {
            a = new MapperArgs(parseFlags([
                '--root', '../tmp/',
                '--data', '/foo/bar.yaml',
                '--mappings', './bat/baz.yaml',
                '--basemap', 'light',
                '--basemap=dark',
                '--size', '1000x500',
                '--size=500x1000x3.14',
                '--ratio', '16x9',
                '--ratio=9x16x15',
                '--padding', '17',
                '--prefix', 'foo-bar-baz-bat'
            ]))
            assertStrictEquals(a.MODE, 'DEFAULT')
            assertStrictEquals(a.root, '../tmp')
            assertStrictEquals(a.dataFileFullPath, '/foo/bar.yaml')
            assertStrictEquals(a.mappingsFileFullPath, './bat/baz.yaml')
            assertEquals(a.basemaps.sort(), ['light-v11', 'dark-v11'].sort())
            assertEquals(a.sizes, [
                ImageSize.fromSize(1000, 500),
                ImageSize.fromSize(500, 1000, 3.14),
                ImageSize.fromRatio(16, 9),
                ImageSize.fromRatio(9, 16, 15),
            ])
            assertStrictEquals(a.padding, 17)
            assertStrictEquals(a.prefix, 'foo-bar-baz-bat')
        })
    })
})

Deno.test('invalid arguments for MapperArgs', async (t) => {
    const testCases: Array<[stepName: string, parseFlagsArgs: Array<string>]> = [
        ['help is no longer an option', ['--help']],

        ['version is no longer an option', ['--version']],

        ['version and help', ['--version', '--help']],

        ['root with empty', ['--root=']],
        ['root with boolean', ['--root']],
        ['root with number', ['--root=1']],
        ['root with valid and invalid', ['--root=/foo/bar', '--root=']],

        ['data with empty', ['--data=']],
        ['data with boolean', ['--data']],
        ['data with number', ['--data=1']],
        ['data with valid and invalid', ['--data=/foo/bar', '--data=']],

        ['mappings with empty', ['--mappings=']],
        ['mappings with boolean', ['--mappings']],
        ['mappings with number', ['--mappings=1']],
        ['mappings with valid and invalid', ['--mappings=/foo/bar', '--mappings=']],

        ['prefix with empty', ['--prefix=']],
        ['prefix with boolean', ['--prefix']],
        ['prefix with decimal', ['--prefix=1.1']],
        ['prefix with valid and invalid', ['--prefix=foo', '--prefix=FOO']],

        ['padding with empty', ['--padding=']],
        ['padding with boolean', ['--padding']],
        ['padding with string', ['--padding', 'foo']],
        ['padding with decimal', ['--padding=1.1']],
        ['padding with valid and invalid', ['--padding=1', '--padding=1.1']],

        ['offset with empty', ['simplify', '--offset=']],
        ['offset with boolean', ['simplify', '--offset']],
        ['offset with string', ['simplify', '--offset', 'foo']],
        ['offset with negative', ['simplify', '--offset=-1']],
        ['offset with valid and invalid', ['simplify', '--offset=1', '--offset=-1']],

        ['size with empty', ['--size=']],
        ['size with boolean', ['--size']],
        ['size with string', ['--size', 'foo']],
        ['size with decimal', ['--size=1.1']],
        ['size with array/object', ['--size=2', '--size=3']],
        ['size with valid and invalid', ['--size=1x1', '--size=1x1x1x1']],
        ['size with invalid', ['--size=1x-1']],
        ['size with invalid', ['--size=1x2560']],
        ['size with invalid', ['--size=2560x1']],
        ['size with invalid', ['--size=0x0']],
        ['size with invalid', ['--size=1x']],
        ['size with invalid', ['--size=x1']],
        ['size with invalid', ['--size=2561x1']],
        ['size with invalid', ['--size=1x2561']],
        ['size with invalid', ['--size=2561x2561']],
        ['size with invalid', ['--size=1x1x-1']],
        ['size with invalid', ['--size=1x1x22.1']],
        ['size with invalid', ['--size=1x1x23']],

        ['ratio with empty', ['--ratio=']],
        ['ratio with boolean', ['--ratio']],
        ['ratio with string', ['--ratio', 'foo']],
        ['ratio with decimal', ['--ratio=1.1']],
        ['ratio with array/object', ['--ratio=2', '--ratio=3']],
        ['ratio with valid and invalid', ['--ratio=1x1', '--ratio=1x1x1x1']],
        ['ratio with invalid', ['--ratio=1x-1']],
        ['ratio with invalid', ['--ratio=0x0']],
        ['ratio with invalid', ['--ratio=1x']],
        ['ratio with invalid', ['--ratio=x1']],
        ['ratio with invalid', ['--ratio=1x1x-1']],
        ['ratio with invalid', ['--ratio=1x1x22.1']],
        ['ratio with invalid', ['--ratio=1x1x23']],

        ['basemap with empty', ['--basemap=']],
        ['basemap with boolean', ['--basemap']],
        ['basemap with string', ['--basemap', 'foo']],
        ['basemap with decimal', ['--basemap=1.1']],
        ['basemap with array/object', ['--basemap=2', '--basemap=3']],
        ['basemap with valid and invalid', ['--basemap=light', '--basemap=light-v11']],
        ['basemap with invalid case', ['--basemap=Light']],

        ['help with an argument', ['help', '--root=foo']],
        ['version with an argument', ['version', '--root=foo']],
        ['upgrade with an argument', ['upgrade', '--root=foo']],

        ['stats with bad argument', ['stats', '--help']],
        ['simplify with bad argument', ['simplify', '--version']],
        ['gpx-to-yaml with bad argument', ['gpx-to-yaml', '--version']],
        ['geojson-to-yaml with bad argument', ['geojson-to-yaml', '--version']],
        ['yaml-to-geojson with bad argument', ['yaml-to-geojson', '--version']],

        ['multiple args supplied', ['foo', 'bar']],
        ['bad command specified', ['foo']],
    ]

    for (const tc of testCases) {
        await t.step(`${tc[0]} - ${JSON.stringify(tc[1])}`, () => { assertThrows(() => new MapperArgs(parseFlags(tc[1]))) })
    }
})
