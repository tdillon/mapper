import { MappingsThing } from '/src/utils/types.ts'

type MapboxRealWorldSize = ['interpolate', ['exponential', 2], ['zoom'], 0, 1, 22, ['*', ['/', number, 3], ['^', 2, 6]]]

// Only needed in this file, I believe.
type MapboxPaint = {
    'line-color': string,
    'line-width': MapboxRealWorldSize
} | {
    'circle-color': string,
    'circle-radius': MapboxRealWorldSize,
    'circle-stroke-color': string,
    'circle-stroke-width': MapboxRealWorldSize
} | {
    'fill-color': string,
    'fill-outline-color': string
}

/**
 *
 * @param m
 * @returns
 */
export function getStyleTemplate(m: MappingsThing) {
    let paint: MapboxPaint

    if (!m.paint) {
        m.paint = {}
    }

    switch (m.type) {
        case 'point':
            paint = {
                'circle-color': m.paint.color ?? '#ff7900',  // Safety Orange
                'circle-radius': ['interpolate', ['exponential', 2], ['zoom'], 0, 1, 22, ['*', ['/', (m.paint.diameter ?? 3) / 2, 3], ['^', 2, 6]]],  // mapbox expects a radius, so divide by 2
                'circle-stroke-color': m.paint['stroke-color'] ?? 'transparent',
                'circle-stroke-width': ['interpolate', ['exponential', 2], ['zoom'], 0, 1, 22, ['*', ['/', m.paint['stroke-width'] ?? 0, 3], ['^', 2, 6]]]
            }
            break;
        case 'area':
            paint = {
                'fill-color': m.paint.fill ?? '#88ff00',  // Chartreuse
                'fill-outline-color': m.paint.border ?? 'transparent'
            }
            break;
        case 'line':
            paint = {
                'line-color': m.paint.color ?? '#0087ff',  // Azure Blue
                'line-width': ['interpolate', ['exponential', 2], ['zoom'], 0, 1, 22, ['*', ['/', m.paint.width ?? 12, 3], ['^', 2, 6]]]
            }
            break;
    }

    return {
        id: m.name,
        type: ({ area: 'fill', line: 'line', point: 'circle' })[m.type],
        paint,
        filter: [
            'all',
            [
                'match',
                [
                    'geometry-type'
                ],
                [
                    ({ area: 'Polygon', line: 'LineString', point: 'Point' })[m.type]
                ],
                true,
                false
            ],
            [
                'match',
                [
                    'get',
                    'name'
                ],
                m.objects.map(o => `${m.name} - ${o}`),
                true,
                false
            ]
        ],
        layout: m.type === 'line' ? {
            'line-cap': 'round',
            'line-join': 'round'
        } : {},
        source: 'composite',
        'source-layer': 'everything-layer',
    }
}
