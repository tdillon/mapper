import { assertStrictEquals } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'

Deno.test('reading environment variables', async () => {
    const myStub = stub(Deno.env, 'get', returnsNext([undefined, undefined]))
    try {
        const config = await import('/src/utils/config.ts')
        assertStrictEquals(config.TOKEN, '')
        assertStrictEquals(config.USERNAME, '')
    } finally {
        myStub.restore()
    }
})
