import type { Args } from 'std/cli/parse_args.ts'
import { basename, resolve } from 'std/path/mod.ts'
import { ImageSize } from '/src/utils/image-size.ts'
import type { ARG_TYPE, MapboxOwnedStyle } from '/src/utils/types.ts'

export class MapperArgs {

    MODE: 'DEFAULT' | 'HELP' | 'VERSION' | 'STATS' | 'SIMPLIFY' | 'UPGRADE' |
        'GEOJSON-TO-YAML' | 'GPX-TO-YAML' | 'YAML-TO-GEOJSON' | undefined

    static readonly DEFAULT_ROOT = '.'
    static readonly DEFAULT_DATA_FILE_NAME = 'data.yaml'
    static readonly DEFAULT_MAPPINGS_FILE_NAME = 'mappings.yaml'
    static readonly DEFAULT_SIZE: Array<ImageSize> = []
    static readonly DEFAULT_RATIO: Array<ImageSize> = [
        ImageSize.fromRatio(1, 1),
        ImageSize.fromRatio(1, 1, 15),
        ImageSize.fromRatio(8.5, 11),
        ImageSize.fromRatio(11, 8.5),
    ]
    static readonly DEFAULT_PADDING = 20
    static readonly DEFAULT_BASEMAP: Array<MapboxOwnedStyle> = [
        'dark-v11',
        'light-v11',
        'outdoors-v12',
        'satellite-v9',
    ]
    static readonly MAPBOX_STYLES: Array<MapboxOwnedStyle> = [
        'dark-v11',
        'light-v11',
        'navigation-day-v1',
        'navigation-night-v1',
        'outdoors-v12',
        'satellite-streets-v12',
        'satellite-v9',
        'streets-v12',
    ]
    /** Array of allowed basemap argument values. e.g., ['dark', 'light', 'satellite-streets', etc.] */
    static readonly ALLOWED_BASEMAP_ARGS = MapperArgs.MAPBOX_STYLES.map(s => [...s.matchAll(/(.+)-v[0-9]*/g)][0][1]).sort()

    /** The default offset for the `simplify` command in feet */
    static readonly DEFAULT_OFFSET = 1

    readonly basemaps: Array<MapboxOwnedStyle>
    readonly sizes: Array<ImageSize>

    /** All output will be written to this directory.  Stored without any trailing forward-slashes. */
    readonly root: string
    readonly dataFileFullPath: string
    readonly mappingsFileFullPath: string
    readonly prefix: string
    readonly padding: number
    readonly offset: number

    /**
     * @example
     * const a = new MapperArgs(parseFlags(Deno.args, { collect: ['basemap', 'size', 'ratio'] }))
     *
     * @example
     * // for testing, we can inject various command line args :)
     * const a = new MapperArgs(parseFlags(['--root', '/foo/bar'], { collect: ['basemap', 'size', 'ratio'] }))
     */
    constructor(private _: Args) {
        this.checkArgs(this._)

        this.basemaps = this.getBasemaps(this._.basemap)

        this.sizes = [
            ...this.getSize(this._.size, ImageSize.fromSize),
            ...this.getSize(this._.ratio, ImageSize.fromRatio)
        ]

        if (this.sizes.length === 0) {  // Only use defaults for SIZE and RATIO if no arguments are given.
            this.sizes = [...MapperArgs.DEFAULT_SIZE, ...MapperArgs.DEFAULT_RATIO]
        }

        this.root = this.getRoot(this._.root)
        this.dataFileFullPath = this.getDataFileFullPath(this._.data)
        this.mappingsFileFullPath = this.getMappingsFileFullPath(this._.mappings)
        this.prefix = this.getPrefix(this._.prefix)
        this.padding = this.getPadding(this._.padding)
        this.offset = this.getOffset(this._.offset)
    }

    private checkArgs(args: Args) {
        const allowedDefaultArgs = ['root', 'data', 'mappings', 'basemap', 'size', 'ratio', 'padding', 'prefix']
        const allowedStatsArgs = ['root', 'data', 'mappings']
        const allowedSimplifyArgs = ['root', 'data', 'mappings', 'offset']
        const allowedGpxToYamlArgs = ['root']
        const allowedGeoJSONToYamlArgs = ['root']
        const allowedYamlToGeoJSONArgs = ['root', 'data', 'mappings']
        const providedArgs = Object.keys(args).filter(a => a !== '_')

        if (args._.length === 1) {
            if (args._[0] === 'help' && providedArgs.length === 0) {
                this.MODE = 'HELP'
            } else if (args._[0] === 'version' && providedArgs.length === 0) {
                this.MODE = 'VERSION'
            } else if (args._[0] === 'upgrade' && providedArgs.length === 0) {
                this.MODE = 'UPGRADE'
            } else if (args._[0] === 'stats' && providedArgs.every(k => allowedStatsArgs.includes(k))) {
                this.MODE = 'STATS'
            } else if (args._[0] === 'simplify' && providedArgs.every(k => allowedSimplifyArgs.includes(k))) {
                this.MODE = 'SIMPLIFY'
            } else if (args._[0] === 'geojson-to-yaml' && providedArgs.every(k => allowedGeoJSONToYamlArgs.includes(k))) {
                this.MODE = 'GEOJSON-TO-YAML'
            } else if (args._[0] === 'gpx-to-yaml' && providedArgs.every(k => allowedGpxToYamlArgs.includes(k))) {
                this.MODE = 'GPX-TO-YAML'
            } else if (args._[0] === 'yaml-to-geojson' && providedArgs.every(k => allowedYamlToGeoJSONArgs.includes(k))) {
                this.MODE = 'YAML-TO-GEOJSON'
            } else {
                throw new Error('Invalid usage.  Run "mapper help" to review valid usage.')
            }
        } else if (args._.length !== 0) {
            throw new Error('Invalid usage.  Run "mapper help" to review valid usage.')
        } else if (providedArgs.every(k => allowedDefaultArgs.includes(k))) {
            this.MODE = 'DEFAULT'
        } else {
            throw new Error('Invalid usage.  Run "mapper help" to review valid usage.')
        }

        return true
    }

    private getRoot(args: ARG_TYPE) {
        if (typeof args === 'undefined') {
            return MapperArgs.DEFAULT_ROOT
        } else if (typeof args === 'string' && args.replace(/\/*$/, '') !== '') {
            return args.replace(/\/*$/, '')
        } else {
            throw new Error(`\nInvalid '--root' usage.\nYou provided ${JSON.stringify(args)}\nThe value must be a string.`);
        }
    }

    private getDataFileFullPath(args: ARG_TYPE) {
        if (typeof args === 'undefined') {
            return `${this.root}/${MapperArgs.DEFAULT_DATA_FILE_NAME}`
        } else if (typeof args === 'string' && args) {
            return args
        } else {
            throw new Error(`\nInvalid '--data' usage.\nYou provided ${JSON.stringify(args)}\nThe value must be a string.`);
        }
    }

    private getMappingsFileFullPath(args: ARG_TYPE) {
        if (typeof args === 'undefined') {
            return `${this.root}/${MapperArgs.DEFAULT_MAPPINGS_FILE_NAME}`
        } else if (typeof args === 'string' && args) {
            return args
        } else {
            throw new Error(`\nInvalid '--mappings' usage.\nYou provided ${JSON.stringify(args)}\nThe value must be a string.`);
        }
    }

    private getPrefix(args: ARG_TYPE) {
        const kabobCase = /^[a-z0-9\-]+$/

        if (typeof args === 'undefined') {
            return basename(resolve(this.root)).toLowerCase().replaceAll(' ', '-')
        } else if (typeof args === 'string' && kabobCase.test(args)) {
            return args
        } else {
            throw new Error(`\nInvalid '--prefix' usage.\nYou provided ${JSON.stringify(args)}\nThe value must be a kabob-cased string.`);
        }
    }

    private getPadding(args: ARG_TYPE) {
        if (typeof args === 'undefined') {
            return MapperArgs.DEFAULT_PADDING
        } else if (typeof args === 'number' && args >= 0 && Number.isInteger(args)) {
            return args
        } else {
            throw new Error(`\nInvalid '--padding' usage.\nYou provided ${JSON.stringify(args)}\nThe value must be a non-negative integer.`);
        }
    }

    private getOffset(args: ARG_TYPE) {
        if (typeof args === 'undefined') {
            return MapperArgs.DEFAULT_OFFSET
        } else if (typeof args === 'number' && args >= 0) {
            return args
        } else {
            throw new Error(`\nInvalid '--offset' usage.\nYou provided ${JSON.stringify(args)}\nThe value must be a non-negative number.`);
        }
    }

    private getSize(args: ARG_TYPE, func: typeof ImageSize.fromSize | typeof ImageSize.fromRatio): Array<ImageSize> {
        let retVal: Array<ImageSize> = []

        if (typeof args === 'object') {  // an array of strings
            const pattern = /^(\d*\.*\d+)[xX](\d*\.*\d+)(?:[xX](\d*\.*\d+))?$/
            retVal = args.map(arg => <string[]>arg.toString().match(pattern)).map(x => func(+x[1], +x[2], typeof x[3] === 'undefined' ? undefined : +x[3]))
        }

        return retVal
    }

    private getBasemaps(args: ARG_TYPE) {
        let retVal: Array<MapboxOwnedStyle>

        if (typeof args === 'object') {  // At this point, args should be an array
            if (args.every(b => MapperArgs.ALLOWED_BASEMAP_ARGS.includes(b.toString()))) {
                retVal = [...new Set(<Array<MapboxOwnedStyle>>args.map(b => MapperArgs.MAPBOX_STYLES.find(s => [...s.matchAll(/(.+)-v[0-9]*/g)][0][1] === b)))]  // Get unique set of basemaps
            } else {
                throw new Error(`\nInvalid '--basemap' option(s).\nYou provided ${JSON.stringify(args)} but the allowed values are:\n\t${MapperArgs.ALLOWED_BASEMAP_ARGS.join('\n\t')}\n`);
            }
        } else {  // undefined since no --basemap option was supplied, use default
            retVal = MapperArgs.DEFAULT_BASEMAP
        }

        return retVal
    }

}
