import { delay } from "std/async/delay.ts";

/**
 * Use this decorator to automatically retry failed API calls.
 * @param numRetries - number of retry attempts
 * @param delaySec - number of seconds to wait between retry attempts
 */
export function retry(numRetries = 10, delaySec = 10) {
    return function (_target: unknown, _key: string | symbol, descriptor: PropertyDescriptor) {
        const original = descriptor.value

        if (typeof original === 'undefined') {
            throw new Error('This should never happen: [descriptor value is undefined].')
        }

        // deno-lint-ignore no-explicit-any
        descriptor.value = async function (...args: any) {
            do {
                try {
                    return await original.apply(this, args);
                } catch (error) {
                    --numRetries
                    console.log(`Mapbox API request threw an error.`)
                    console.log(error)
                    console.log(`Retrying in ${delaySec} seconds.`)
                    await delay(delaySec * 1000)
                }
            } while (numRetries >= 0);

            throw new Error('Mapbox API was unsuccessful after all retries.')
        }

        return descriptor
    }
}
