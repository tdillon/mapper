import { assert, assertEquals, assertStrictEquals, assertThrows } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { _getFeatures, _validateDataFileContent, _validateMappingsFileContent, getDataFileContent, getGeoJSONContent, getMappingsFileContent } from '/src/utils/helpers.ts'
import { DataYaml, MappingsThing } from '/src/utils/types.ts'

// Here is how to run tests to get coverage specifically from this file.
// rm -rf coverage/ && deno test --allow-read=. --allow-env=MAPBOX_USERNAME,MAPBOX_TOKEN --coverage --cached-only --junit-path=coverage/junit.xml src/utils/helpers.test.ts && deno task coverage-html
// Open coverage/html/helpers.ts.html in browser.

Deno.test('helpers.ts - _validateDataFileContent', async (t) => {

  await t.step('undefined dataFileContent', () => {
    assertThrows(() => _validateDataFileContent(undefined as unknown as DataYaml), Error, 'Invalid data file.  The file must have content:')
  })

  await t.step('correct dataFileContent', () => {
    assertStrictEquals(_validateDataFileContent({ a: [[0, 0]] }), undefined)
  })

  await t.step('invalid dataFileContent - missing array', () => {
    assertThrows(() => _validateDataFileContent({ a: null } as unknown as DataYaml), Error, 'does not have an array of longitude/latitudes:')
  })

  await t.step('invalid dataFileContent - not an object', () => {
    assertThrows(() => _validateDataFileContent({ a: 1 } as unknown as DataYaml), Error, 'does not have an array of longitude/latitudes:')
  })

  await t.step('invalid dataFileContent - not an array', () => {
    assertThrows(() => _validateDataFileContent({ a: { lat: 0, lon: 0 } } as unknown as DataYaml), Error, 'does not have an array of longitude/latitudes:')
  })

  await t.step('invalid dataFileContent - empty array', () => {
    assertThrows(() => _validateDataFileContent({ a: [] } as unknown as DataYaml), Error, 'does not have an array of longitude/latitudes:')
  })

  await t.step('invalid dataFileContent - array length === 0', () => {
    assertThrows(() => _validateDataFileContent({ a: [[]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - array length === 1', () => {
    assertThrows(() => _validateDataFileContent({ a: [[0]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - array length === 3', () => {
    assertThrows(() => _validateDataFileContent({ a: [[1, 2, 3]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - lon not a number', () => {
    assertThrows(() => _validateDataFileContent({ a: [['1', 1]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - lat not a number', () => {
    assertThrows(() => _validateDataFileContent({ a: [[1, '1']] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - lon < -180', () => {
    assertThrows(() => _validateDataFileContent({ a: [[-180.1, 0]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - lon > 180', () => {
    assertThrows(() => _validateDataFileContent({ a: [[180.1, 0]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - lat < -90', () => {
    assertThrows(() => _validateDataFileContent({ a: [[0, -90.1]] } as unknown as DataYaml), Error, 'has bad data:')
  })

  await t.step('invalid dataFileContent - lat > 90', () => {
    assertThrows(() => _validateDataFileContent({ a: [[0, 90.1]] } as unknown as DataYaml), Error, 'has bad data:')
  })

})

Deno.test('helpers.ts - _validateMappingsFileContent', async (t) => {

  await t.step('undefined mappingsFileContent', () => {
    assertThrows(() => _validateMappingsFileContent(undefined as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  The file does not have an array of proper objects:')
  })
  await t.step('non object mappingsFileContent', () => {
    assertThrows(() => _validateMappingsFileContent(3 as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  The file does not have an array of proper objects:')
  })
  await t.step('object without length property mappingsFileContent', () => {
    assertThrows(() => _validateMappingsFileContent({ name: 'bob' } as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  The file does not have an array of proper objects:')
  })
  await t.step('empty array mappingsFileContent', () => {
    assertThrows(() => _validateMappingsFileContent([], undefined as unknown as DataYaml), Error, 'Invalid mappings file.  The file does not have an array of proper objects:')
  })
  await t.step('duplicate names of mappings', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a' }, { name: 'a' }] as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Mapping object names must be unique:')
  })
  await t.step('duplicate names of mappings', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a' }, { name: 'a' }] as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Mapping object names must be unique:')
  })
  await t.step('no name property', () => {
    assertThrows(() => _validateMappingsFileContent([{ NAME: 'a' }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have name property which is a string:')
  })
  await t.step('name property must be a string', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 0 }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have name property which is a string:')
  })
  await t.step('no type property', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a' }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have type property which is (line | point | area):')
  })
  await t.step('type property must have correct value', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'a' }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have type property which is (line | point | area):')
  })
  await t.step('no objects property', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line' }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have objects property which is an array of strings:')
  })
  await t.step('objects property must be an object', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: 3 }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have objects property which is an array of strings:')
  })
  await t.step('objects property must be an array', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: { name: 'a' } }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have objects property which is an array of strings:')
  })
  await t.step('objects property must be non-empty array', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: [] }] as unknown as Array<MappingsThing>, undefined as unknown as DataYaml), Error, 'Invalid mappings file.  Each object must have objects property which is an array of strings:')
  })
  await t.step('mappings objects must be in data file', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'] }], { b: [] }), Error, 'Invalid mappings file.  Each object\'s objects array must include items from data yaml file:')
  })
  await t.step('correct mappings file', () => {
    assertStrictEquals(_validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'] }], { a: [] }), undefined)
  })
  await t.step('mappings file has correct properties', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'], PAINT: 3 }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Each object must have (name, type, objects) and optionally (paint) properties:')
  })

  await t.step('area paint must be an object', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'area', objects: ['a'], paint: 'a' }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  paint must be an object:')
  })
  await t.step('area paint must have correct properties', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'area', objects: ['a'], paint: { FILL: 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for areas can optionally have fill and border:')
  })
  await t.step('area paint.border must be a string', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'area', objects: ['a'], paint: { border: 0, fill: '' } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for areas can optionally have fill and border:')
  })
  await t.step('area paint.fill must be a string', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'area', objects: ['a'], paint: { border: '', fill: 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for areas can optionally have fill and border:')
  })

  await t.step('line paint must be an object', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'], paint: 'a' }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  paint must be an object:')
  })
  await t.step('line paint must have correct properties', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'], paint: { COLOR: 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for lines can optionally have color and width:')
  })
  await t.step('line paint.color must be a string', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'], paint: { color: 0, width: 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for lines can optionally have color and width:')
  })
  await t.step('line paint.width must be a number', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'line', objects: ['a'], paint: { color: '', width: '' } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for lines can optionally have color and width:')
  })

  await t.step('point paint must be an object', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'point', objects: ['a'], paint: 'a' }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  paint must be an object:')
  })
  await t.step('point paint must have correct properties', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'point', objects: ['a'], paint: { COLOR: 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for points can optionally have color, diameter, stroke-color, and stroke-width:')
  })
  await t.step('point paint.color must be a string', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'point', objects: ['a'], paint: { color: 0, diameter: 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for points can optionally have color, diameter, stroke-color, and stroke-width:')
  })
  await t.step('point paint.diameter must be a number', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'point', objects: ['a'], paint: { color: '', diameter: '' } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for points can optionally have color, diameter, stroke-color, and stroke-width:')
  })
  await t.step('point paint.stroke-color must be a string', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'point', objects: ['a'], paint: { color: '', diameter: 0, 'stroke-color': 0 } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for points can optionally have color, diameter, stroke-color, and stroke-width:')
  })
  await t.step('point paint.stroke-width must be a number', () => {
    assertThrows(() => _validateMappingsFileContent([{ name: 'a', type: 'point', objects: ['a'], paint: { color: '', diameter: 0, 'stroke-color': '', 'stroke-width': '' } }] as unknown as Array<MappingsThing>, { a: [] }), Error, 'Invalid mappings file.  Paint for points can optionally have color, diameter, stroke-color, and stroke-width:')
  })
})

Deno.test('helpers.ts - getDataFileContent', async (t) => {

  const validDataFiles = [
    `a: [[0,0]]`
  ]

  const invalidDataFiles = [
    `a:`,
    'a:[[0,0]]',
    'a: []',
    'a: [[]]',
    'a: [[0]]',
    'a: [[0,1,2]]',
    'a: [[-180.1,0]]',
    'a: [[180.1,0]]',
    'a: [[0,-90.1]]',
    'a: [[0,90.1]]',
    'a: [["0",0]]',
    'a: [[0,"0"]]',
    'a: [["0","0"]]',
    'a: 3',
    'a: string',
    'a: [0,0]',
    `# Empty file`
  ]

  let myStub = stub(Deno, 'readTextFileSync', returnsNext(validDataFiles))

  try {
    for (const _ of validDataFiles) {
      await t.step('valid data file', () => {
        assert(getDataFileContent('') !== undefined)
      })
    }
  } finally {
    myStub.restore()
  }

  myStub = stub(Deno, 'readTextFileSync', returnsNext(invalidDataFiles))

  try {
    for (const _ of invalidDataFiles) {
      await t.step('invalid data file', () => {
        assertThrows(() => getDataFileContent(''), Error)
      })
    }
  } finally {
    myStub.restore()
  }

})

Deno.test('helpers.ts - getMappingsFileContent', async (t) => {

  const validMappingsFiles = [
    `# BEGIN AREA
- name: foo
  type: area
  objects: [a]`,
    `
- name: foo
  type: area
  objects: [a]
  paint:`,
    `
- name: foo
  type: area
  objects: [a]
  paint:
    border: string
    fill: string`,
    `
- name: foo
  type: area
  objects: [a]
  paint:
    border: string`,
    `
- name: foo
  type: area
  objects: [a]
  paint:
    fill: string`,

    `# BEGIN LINE
- name: foo
  type: line
  objects: [a]`,
    `
- name: foo
  type: line
  objects: [a]
  paint:`,
    `
- name: foo
  type: line
  objects: [a]
  paint:
    color: string
    width: 0`,
    `
- name: foo
  type: line
  objects: [a]
  paint:
    color: string`,
    `
- name: foo
  type: line
  objects: [a]
  paint:
    width: 0`
  ]

  const invalidMappingsFiles = [
    `# bad indention
- name: foo
    type: point
    objects:
      - a`,

    `# no high level array
name: foo
type: point
objects: [a]
`,

    `# no space after hyphen
-name: a
 type: line
 objects: [a]
`,

    `# roperties are case sensitive
- Name: a
  type: line
  objects: [a]
`,

    `# name is a string
- name: []
`,

    `# duplicate names
- name: a
  type: line
  objects: [a]
- name: a
  type: area
  objects: [a]`,

    `# bad types
- name: a
  type: Line
  objects: [a]`,

    `# missing type property
  - name: a
    objects: [a]`,

    `# missing objects property
- name: a
  type: line`,

    `# bad objects item
- name: a
  type: line
  objects: [A]`,

    `# bad property name
- name: a
  type: line
  objects: [a]
  Paint:
    color: pink`,

    `# paint isn't an object
- name: foo
  type: point
  objects: [a]
  paint: string
    `,

    `# area paint must be an object
- name: foo
  type: area
  objects: [a]
  paint:
    foo: bar
`,

    `# area paint border must be a string
- name: foo
  type: area
  objects: [a]
  paint:
    border: [3]
    fill: string
`,

    `# area paint fill must be a string
- name: foo
  type: area
  objects: [a]
  paint:
    border: string
    fill: [3]
`,

    `# line paint must be an object
- name: foo
  type: line
  objects: [a]
  paint:
    foo: bar
`,

    `# line paint color must be a string
- name: foo
  type: line
  objects: [a]
  paint:
    color: [3]
    width: 1
`,

    `# line paint width must be a number
- name: foo
  type: line
  objects: [a]
  paint:
    color: string
    width: string
`,

    `# point paint must be an object
- name: foo
  type: point
  objects: [a]
  paint:
    foo: bar
`,

    `# point paint color must be a string
- name: foo
  type: point
  objects: [a]
  paint:
    color: [string]
    diameter: 1
    stroke-color: string
    stroke-width: 1
`,

    `# point paint diameter must be a number
- name: foo
  type: point
  objects: [a]
  paint:
    color: string
    diameter: [1]
    stroke-color: string
    stroke-width: 1
`,

    `# point paint stroke-color must be a string
- name: foo
  type: point
  objects: [a]
  paint:
    color: string
    diameter: 1
    stroke-color: [string]
    stroke-width: 1
`,

    `# point paint stroke-width must be a number
- name: foo
  type: point
  objects: [a]
  paint:
    color: string
    diameter: 1
    stroke-color: string
    stroke-width: [1]
`,

    `# Empty file`
  ]

  let myStub = stub(Deno, 'readTextFileSync', returnsNext(validMappingsFiles))

  try {
    for (const _ of validMappingsFiles) {
      await t.step('valid mappings file', () => {
        assert(getMappingsFileContent('', { a: [] }).length > 0)
      })
    }
  } finally {
    myStub.restore()
  }

  myStub = stub(Deno, 'readTextFileSync', returnsNext(invalidMappingsFiles))

  try {
    for (const _ of invalidMappingsFiles) {
      await t.step('invalid mappings file', () => {
        assertThrows(() => getMappingsFileContent('', { a: [] }), Error)
      })
    }
  } finally {
    myStub.restore()
  }

})

Deno.test('helpers.ts - _getFeatures', async (t) => {

  await t.step('test point feature w/o color', () => {
    const f = _getFeatures({ DataName: [[0, 0]] }, [{
      name: 'MappingName',
      type: 'point',
      objects: ['DataName']
    }])
    assertStrictEquals(f.length, 1)
    assertStrictEquals(f.at(0)?.type, 'Feature')
    assertStrictEquals(f.at(0)?.properties.name, 'MappingName - DataName')
    assertStrictEquals(f.at(0)?.properties.dataName, 'DataName')
    assertStrictEquals(f.at(0)?.properties.mappingName, 'MappingName')
    assertStrictEquals(f.at(0)?.properties["marker-color"], undefined)
    assertStrictEquals(f.at(0)?.properties["marker-size"], 'small')
    assertStrictEquals(f.at(0)?.geometry.type, 'MultiPoint')
    assertEquals(f.at(0)?.geometry.coordinates, [[0, 0]])
  })

  await t.step('test point feature w/ color', () => {
    const f = _getFeatures({ DataName: [[0, 0]] }, [{
      name: 'MappingName',
      type: 'point',
      objects: ['DataName'],
      paint: { color: 'blue' }
    }])
    assertStrictEquals(f.length, 1)
    assertStrictEquals(f.at(0)?.type, 'Feature')
    assertStrictEquals(f.at(0)?.properties.name, 'MappingName - DataName')
    assertStrictEquals(f.at(0)?.properties.dataName, 'DataName')
    assertStrictEquals(f.at(0)?.properties.mappingName, 'MappingName')
    assertStrictEquals(f.at(0)?.properties["marker-color"], 'blue')
    assertStrictEquals(f.at(0)?.properties["marker-size"], 'small')
    assertStrictEquals(f.at(0)?.geometry.type, 'MultiPoint')
    assertEquals(f.at(0)?.geometry.coordinates, [[0, 0]])
  })

  await t.step('test line feature w/o color', () => {
    const f = _getFeatures({ DataName: [[0, 0], [1, 1]] }, [{
      name: 'MappingName',
      type: 'line',
      objects: ['DataName']
    }])
    assertStrictEquals(f.length, 1)
    assertStrictEquals(f.at(0)?.type, 'Feature')
    assertStrictEquals(f.at(0)?.properties.name, 'MappingName - DataName')
    assertStrictEquals(f.at(0)?.properties.dataName, 'DataName')
    assertStrictEquals(f.at(0)?.properties.mappingName, 'MappingName')
    assertStrictEquals(f.at(0)?.properties.stroke, undefined)
    assertStrictEquals(f.at(0)?.geometry.type, 'LineString')
    assertEquals(f.at(0)?.geometry.coordinates, [[0, 0], [1, 1]])
  })

  await t.step('test line feature w/ color', () => {
    const f = _getFeatures({ DataName: [[0, 0], [1, 1]] }, [{
      name: 'MappingName',
      type: 'line',
      objects: ['DataName'],
      paint: { color: 'blue' }
    }])
    assertStrictEquals(f.length, 1)
    assertStrictEquals(f.at(0)?.type, 'Feature')
    assertStrictEquals(f.at(0)?.properties.name, 'MappingName - DataName')
    assertStrictEquals(f.at(0)?.properties.dataName, 'DataName')
    assertStrictEquals(f.at(0)?.properties.mappingName, 'MappingName')
    assertStrictEquals(f.at(0)?.properties.stroke, 'blue')
    assertStrictEquals(f.at(0)?.geometry.type, 'LineString')
    assertEquals(f.at(0)?.geometry.coordinates, [[0, 0], [1, 1]])
  })

  await t.step('test area feature w/o color,fill', () => {
    const f = _getFeatures({ DataName: [[0, 0], [1, 1], [2, 2]] }, [{
      name: 'MappingName',
      type: 'area',
      objects: ['DataName']
    }])
    assertStrictEquals(f.length, 1)
    assertStrictEquals(f.at(0)?.type, 'Feature')
    assertStrictEquals(f.at(0)?.properties.name, 'MappingName - DataName')
    assertStrictEquals(f.at(0)?.properties.dataName, 'DataName')
    assertStrictEquals(f.at(0)?.properties.mappingName, 'MappingName')
    assertStrictEquals(f.at(0)?.properties.stroke, undefined)
    assertStrictEquals(f.at(0)?.properties.fill, undefined)
    assertStrictEquals(f.at(0)?.geometry.type, 'Polygon')
    assertEquals(f.at(0)?.geometry.coordinates, [[[0, 0], [1, 1], [2, 2], [0, 0]]])
  })

  await t.step('test area feature w/ color,fill', () => {
    const f = _getFeatures({ DataName: [[0, 0], [1, 1], [2, 2]] }, [{
      name: 'MappingName',
      type: 'area',
      objects: ['DataName'],
      paint: { border: 'blue', fill: 'red' }
    }])
    assertStrictEquals(f.length, 1)
    assertStrictEquals(f.at(0)?.type, 'Feature')
    assertStrictEquals(f.at(0)?.properties.name, 'MappingName - DataName')
    assertStrictEquals(f.at(0)?.properties.dataName, 'DataName')
    assertStrictEquals(f.at(0)?.properties.mappingName, 'MappingName')
    assertStrictEquals(f.at(0)?.properties.stroke, 'blue')
    assertStrictEquals(f.at(0)?.properties.fill, 'red')
    assertStrictEquals(f.at(0)?.geometry.type, 'Polygon')
    assertEquals(f.at(0)?.geometry.coordinates, [[[0, 0], [1, 1], [2, 2], [0, 0]]])
  })

})

Deno.test('helpers.ts - getGeoJSONContent', async (t) => {

  await t.step('test point feature w/o color', () => {
    assertEquals(getGeoJSONContent({ DataName: [[0, 0]] }, [{
      name: 'MappingName',
      type: 'point',
      objects: ['DataName'],
      paint: { color: 'blue' }
    }]), {
      type: 'FeatureCollection', features: [{
        type: 'Feature',
        properties: {
          name: 'MappingName - DataName',
          dataName: 'DataName',
          mappingName: 'MappingName',
          'marker-color': 'blue',
          'marker-size': 'small'
        },
        geometry: {
          type: 'MultiPoint',
          coordinates: [[0, 0]]
        }
      }]
    })

  })

})