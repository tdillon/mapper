export function getTilesetSourceID(tilesetName: string, username: string) {
  return `mapbox://tileset-source/${username}/${tilesetName}`
}

/**
 * @param tilesetName See {@link Mapbox.createTileset()}
 * @param username Mapbox `username` where the tileset is stored.
 * @returns an object which is passed as `body` to {@link Mapbox.createTileset()}
 */
export function getTilesetRequestBody(tilesetName: string, username: string) {
  return {
    name: `${tilesetName} recipe`,
    recipe: {
      version: 1,
      layers: {
        "everything-layer": {
          source: getTilesetSourceID(tilesetName, username),
          minzoom: 0,
          maxzoom: 16
        }
      }
    }
  }
}
