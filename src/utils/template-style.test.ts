import { assert } from 'std/assert/mod.ts'
import { getStyleTemplate } from '/src/utils/template-style.ts'

Deno.test('basic template-style functionality', () => {
    assert(getStyleTemplate({ name: 'foo', objects: ['foo'], type: 'point' }))
    assert(getStyleTemplate({ name: 'foo', objects: ['foo'], type: 'line' }))
    assert(getStyleTemplate({ name: 'foo', objects: ['foo'], type: 'area' }))
})