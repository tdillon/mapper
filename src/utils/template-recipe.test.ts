import { assertStrictEquals } from 'std/assert/mod.ts'
import { getTilesetRequestBody, getTilesetSourceID } from '/src/utils/template-recipe.ts'

Deno.test('getTemplateRecipe', () => {
    const tilesetName = 'foo'
    const username = 'bar'

    const recipe = getTilesetRequestBody(tilesetName, username)

    assertStrictEquals(getTilesetSourceID(tilesetName, username), `mapbox://tileset-source/${username}/${tilesetName}`)
    assertStrictEquals(recipe.name, `${tilesetName} recipe`)
    assertStrictEquals(recipe.recipe.layers['everything-layer'].source, getTilesetSourceID(tilesetName, username))
})
