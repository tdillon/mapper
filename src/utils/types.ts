/**
 * Response object returned from `GET` `/styles/v1/{username}/{style_id}`
 * https://docs.mapbox.com/api/maps/styles/#retrieve-a-style
 */
export interface RetrieveAStyleResponse {
    name: string,
    created?: string,
    modified?: string,
    id?: string,
    owner?: string,
    draft?: string,
    bearing?: string,
    pitch?: string,
    sources: { composite: { url: string, type: string } },
    // deno-lint-ignore no-explicit-any
    layers: Array<any>
}

/**
 * Possible values of the 'stage' property (i.e. status) returned by the job status API.
 * https://docs.mapbox.com/api/maps/mapbox-tiling-service/#retrieve-information-about-a-single-tileset-job
 */
export type JobStatus = 'queued' | 'processing' | 'success' | 'failed'



/**
 * The valid names for Mapbox owned styles.
 *
 * @see {@link https://docs.mapbox.com/api/maps/styles/}
 * */
export type MapboxOwnedStyle =
    'streets-v12' |
    'outdoors-v12' |
    'light-v11' |
    'dark-v11' |
    'satellite-v9' |
    'satellite-streets-v12' |
    'navigation-day-v1' |
    'navigation-night-v1'

export type BoundingBox = [minLongitude: number, minLatitude: number, maxLongitude: number, maxLatitdue: number]

export type LonLatTuple = [longitude: number, latitude: number]

/**
 * This type is used ...
 * YAML file is a
 *
 * @example
 * My road: [[-80.1,30.3],[-80.8,30.4],[-80.7,30.8]]
 * Favorite road 2: [[-80.1,30.3],[-80.8,30.4],[-80.7,30.8]]
 *
 * @example
 * let x:DataYaml = ...
 * x['My road']  // array of longitude/latitude tuples
 */
export interface DataYaml {
    [name: string]: Array<LonLatTuple>;
}

/**
 * The individual properties of parseFlags (i.e., this._.SOMEPROPERTY) have one of these types.
 *
 * typeof will return "object" when an argument is supplied multiple times with a value
 * The actual type in this situation should be Array<string | number>
 *
 * Providing a boolean flag multiple times has no affect
 *
 * Booleans are ignored when they are provided with valued arguments.
 *
 * @example
 * mapper --foo --foo --foo
 * // typeof foo === 'boolean'
 * // this._.foo === true
 *
 * @example
 * mapper --foo --foo=bar --foo 3
 * // typeof foo === 'object' (i.e., Array<string | number>)
 * // this.args._.foo === ['bar', 3]
 *
 * @example
 * mapper
 * // typeof this._.size === 'undefined'
 *
 * mapper --size
 * // typeof this._.size === 'boolean'
 *
 * mapper --size foo
 * // typeof this._.size === 'string'

 * mapper --size 3
 * // typeof this._.size === 'number'
 *
 * mapper --size=foo --size 3
 * // typeof this._.size === 'object' (i.e., Array<string | number>)
 */
export type ARG_TYPE = string | number | boolean | undefined | Array<string | number>

//#region MAPPINGS

interface MappingsLinePaint {
    color?: string
    /** The line will be rendered as `width` feet wide on the map independent of zoom level. */
    width?: number
}

interface MappingsPointPaint {
    color?: string
    /** The point will be rendered as `diameter` feet wide on the map independent of zoom level. */
    diameter?: number
    'stroke-color'?: string
    /** The stroke will be rendered as `stroke-width` feet wide on the map independent of zoom level. */
    'stroke-width'?: number
}

interface MappingsAreaPaint {
    fill?: string,
    border?: string
}

interface MappingsLineElement {
    type: 'line'
    paint?: MappingsLinePaint
}

interface MappingsPointElement {
    type: 'point'
    paint?: MappingsPointPaint
}

interface MappingsAreaElement {
    type: 'area'
    paint?: MappingsAreaPaint
}

export type MappingsTypeAndPaintElement = MappingsLineElement | MappingsPointElement | MappingsAreaElement

/** The objects in mappings.yaml must match this type. */
export type MappingsThing = MappingsTypeAndPaintElement & {
    name: string
    objects: Array<string>
}

//#endregion MAPPINGS



export interface GeoJSON {
    type: 'FeatureCollection',
    features: Array<Feature>
}

export interface Feature {
    type: 'Feature',
    properties: {
        name?: string
        dataName?: string
        mappingName?: string
        /** OPTIONAL used for geojson.io to style `point` */
        'marker-color'?: string
        /** OPTIONAL used for geojson.io to style `point` */
        'marker-size'?: string
        /** OPTIONAL used for geojson.io to style `line` and `area` */
        stroke?: string
        /** OPTIONAL used for geojson.io to style `area` */
        fill?: string
    },
    geometry: {
        type: 'Polygon',
        coordinates: Array<Array<LonLatTuple>>
    } | {
        type: 'LineString'
        coordinates: Array<LonLatTuple>
    } | {
        type: 'MultiPoint',
        coordinates: Array<LonLatTuple>
    } | {
        type: 'Point',
        coordinates: LonLatTuple
    }
}
