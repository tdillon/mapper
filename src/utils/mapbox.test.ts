import { assertEquals, assertRejects, assertStrictEquals } from 'std/assert/mod.ts'
import { returnsNext, stub } from 'std/testing/mock.ts'
import { ImageSize } from '/src/utils/image-size.ts'
import { Mapbox } from '/src/utils/mapbox.ts'
import { JobStatus } from '/src/utils/types.ts'

Deno.test('mapbox api testing', async (t) => {
    /**
     * Stub simplified Mapbox API responses.
     * In most cases, handpick the properties/values you care about and empty the rest.
     */
    const mapboxFetchStub = stub(window, 'fetch', returnsNext([
        // getStyle
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res({ name: 'TEST' }))  // Ignoring everything but name for now.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // getAllStyles
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res([{ id: '1', name: 'foo' }, { id: '2', name: 'bar' }]))  // getAllStyles only cares about id and name currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // deleteStyle
        new Promise(res => res({
            status: 204
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // createStyle
        new Promise(res => res({
            status: 201,
            json: () => new Promise(res => res({ id: '1' }))  // createStyle only cares about id currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // getAllTilesets
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res([{ id: '1' }, { id: '2' }, { id: '3' }]))  // createStyle only cares about id currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // deleteTileset
        new Promise(res => res({
            status: 200
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // createTileset
        new Promise(res => res({
            status: 200
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // publishTileset
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res({ jobId: '1' }))  // publishTileset only cares about jobId currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // getJobStatus
        new Promise(res => res({
            status: 200,
            json: () => new Promise<{ stage: JobStatus }>(res => res({ stage: 'queued' }))  // getJobStatus only cares about stage currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // getAllTilesetSources
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res([{ id: '1' }, { id: '2' }, { id: '3' }]))  // getAllTilesetSources only cares about id currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // deleteTilesetSource
        new Promise(res => res({
            status: 204
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // createTilesetSource
        new Promise(res => res({
            status: 200
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // getQueue
        new Promise(res => res({
            status: 200,
            json: () => new Promise(res => res({ total: 0 }))  // getQueue only cares about total currently.
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),

        // getImage
        new Promise(res => res({
            status: 200,
            arrayBuffer: () => new Promise(res => res(new Uint8Array([0])))
        } as Response)),
        new Promise(res => res({
            status: 500,
            text: () => new Promise(res => res('FAKE ERROR MESSAGE'))
        } as Response)),
    ]))

    try {
        // getStyle
        await t.step('getStyle successful', async () => {
            assertEquals((await Mapbox.getStyle('styleId')).name, 'TEST')
        })
        await t.step('getStyle exception', () => {
            assertRejects(async () => await Mapbox.getStyle('styleId'), Error, 'FAKE ERROR MESSAGE')
        })

        // getAllStyles
        await t.step('getAllStyles successful', async () => {
            const allStyles = await Mapbox.getAllStyles()
            assertEquals(allStyles.length, 2)
            assertEquals(allStyles.at(0), { id: '1', name: 'foo' })
            assertEquals(allStyles.at(1), { id: '2', name: 'bar' })
        })
        await t.step('getAllStyles exception', () => {
            assertRejects(async () => await Mapbox.getAllStyles(), Error, 'FAKE ERROR MESSAGE')
        })

        // deleteStyle
        await t.step('deleteStyle successful', async () => {
            assertStrictEquals(await Mapbox.deleteStyle('myStyleId'), undefined)
        })
        await t.step('deleteStyle exception', () => {
            assertRejects(async () => await Mapbox.deleteStyle('myStyleId'), Error, 'FAKE ERROR MESSAGE')
        })

        // createStyle
        await t.step('createStyle successful', async () => {
            assertEquals((await Mapbox.createStyle('myStyleObject')), '1')
        })
        await t.step('createStyle exception', () => {
            assertRejects(async () => await Mapbox.createStyle('myStyleObject'), Error, 'FAKE ERROR MESSAGE')
        })

        // getAllTilesets
        await t.step('getAllTilesets successful', async () => {
            assertEquals((await Mapbox.getAllTilesets()), ['1', '2', '3'])
        })
        await t.step('getAllTilesets exception', () => {
            assertRejects(async () => await Mapbox.getAllTilesets(), Error, 'FAKE ERROR MESSAGE')
        })

        // deleteTileset
        await t.step('deleteTileset successful', async () => {
            assertStrictEquals(await Mapbox.deleteTileset('myTilesetId'), undefined)
        })
        await t.step('deleteTileset exception', () => {
            assertRejects(async () => await Mapbox.deleteTileset('myTilesetId'), Error, 'FAKE ERROR MESSAGE')
        })

        // createTileset
        await t.step('createTileset successful', async () => {
            assertStrictEquals(await Mapbox.createTileset('myTilesetName', 'myBody'), undefined)
        })
        await t.step('createTileset exception', () => {
            assertRejects(async () => await Mapbox.createTileset('myTilesetName', 'myBody'), Error, 'FAKE ERROR MESSAGE')
        })

        // publishTileset
        await t.step('publishTileset successful', async () => {
            assertEquals((await Mapbox.publishTileset('myTilesetName')), '1')
        })
        await t.step('publishTileset exception', () => {
            assertRejects(async () => await Mapbox.publishTileset('myTilesetName'), Error, 'FAKE ERROR MESSAGE')
        })

        // getJobStatus
        await t.step('getJobStatus successful', async () => {
            assertEquals((await Mapbox.getJobStatus('myJobId', 'myTilesetName')), 'queued')
        })
        await t.step('getJobStatus exception', () => {
            assertRejects(async () => await Mapbox.getJobStatus('myJobId', 'myTilesetName'), Error, 'FAKE ERROR MESSAGE')
        })

        // getAllTilesetSources
        await t.step('getAllTilesetSources successful', async () => {
            assertEquals((await Mapbox.getAllTilesetSources()), ['1', '2', '3'])
        })
        await t.step('getAllTilesetSources exception', () => {
            assertRejects(async () => await Mapbox.getAllTilesetSources(), Error, 'FAKE ERROR MESSAGE')
        })

        // deleteTilesetSource
        await t.step('deleteTilesetSource successful', async () => {
            assertStrictEquals(await Mapbox.deleteTilesetSource('myId'), undefined)
        })
        await t.step('deleteTilesetSource exception', () => {
            assertRejects(async () => await Mapbox.deleteTilesetSource('myId'), Error, 'FAKE ERROR MESSAGE')
        })

        // createTilesetSource
        await t.step('createTilesetSource successful', async () => {
            assertStrictEquals(await Mapbox.createTilesetSource('myId', new FormData()), undefined)
        })
        await t.step('createTilesetSource exception', () => {
            assertRejects(async () => await Mapbox.createTilesetSource('myId', new FormData()), Error, 'FAKE ERROR MESSAGE')
        })

        // getQueue
        await t.step('getQueue successful', async () => {
            assertEquals((await Mapbox.getQueue()), 0)
        })
        await t.step('getQueue exception', () => {
            assertRejects(async () => await Mapbox.getQueue(), Error, 'FAKE ERROR MESSAGE')
        })

        // getImage
        await t.step('getImage successful', async () => {
            assertEquals((await Mapbox.getImage('myStyleId', ImageSize.fromSize(1, 1), 'myPosition', 1)), new Uint8Array([0]))
        })
        await t.step('getImage exception', () => {
            assertRejects(async () => await Mapbox.getImage('myStyleId', ImageSize.fromRatio(1, 1, 1), 'myPosition'), Error, 'FAKE ERROR MESSAGE')
        })
    }

    finally {
        mapboxFetchStub.restore()
    }
})
