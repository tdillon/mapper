export class ImageSize {

    /** The ratio provided to fromRatio (e.g., 8x10) to at most 4 decimal places */
    private ratio?: { width: number, height: number }
    /** Does the image require the `@2x` flag? */
    readonly twoX: boolean

    private constructor(public readonly width: number, public readonly height: number, public readonly zoom: number | undefined, ratioWidth?: number, ratioHeight?: number) {
        if (ratioWidth && ratioHeight) {
            this.ratio = { width: ratioWidth, height: ratioHeight }
        }
        this.twoX = Math.max(this.height, this.width) > 1280
    }

    public get isBoundingBox(): boolean {
        return typeof this.zoom === 'undefined'
    }

    public get isZoom(): boolean {
        return !this.isBoundingBox
    }

    static fromSize(width: number, height: number, zoom?: number) {
        if (!(
            Number.isInteger(width) &&
            (
                (width >= 1 && width <= 1280) ||
                (width > 1280 && width <= 2560 && width % 2 === 0 && height % 2 === 0)
            ) &&
            Number.isInteger(height) &&
            (
                (height >= 1 && height <= 1280) ||
                (height > 1280 && height <= 2560 && height % 2 === 0 && width % 2 === 0)
            )
        )) {
            throw new Error(`\nInvalid size\n`);
        }
        if (zoom && (zoom < 0 || zoom > 22)) {
            throw new Error(`\nInvalid zoom.  Must be between 0 and 22.  You provided ${zoom}\n`)
        }

        return new ImageSize(width, height, zoom)
    }

    static fromRatio(width: number, height: number, zoom?: number) {
        if (width <= 0) {
            throw new Error(`\nInvalid width provided for ratio.  Must be great than zero.  You provided ${width}.\n`);
        }
        if (height <= 0) {
            throw new Error(`\nInvalid height provided for ratio.  Must be great than zero.  You provided ${height}.\n`);
        }
        if (width !== +width.toFixed(4)) {
            throw new Error(`\nInvalid width provided for ratio.  Use at most 4 decimal places.  You provided ${width}\n`);
        }
        if (height !== +height.toFixed(4)) {
            throw new Error(`\nInvalid height provided for ratio.  Use at most 4 decimal places.  You provided ${height}\n`);
        }
        if (zoom && (zoom < 0 || zoom > 22)) {
            throw new Error(`\nInvalid zoom.  Must be between 0 and 22.\n`)
        }

        return new ImageSize(
            width > height ? 2560 : 2 * Math.round(2560 / height * width / 2),  // ensure dimensions are even
            height > width ? 2560 : 2 * Math.round(2560 / width * height / 2),  // ensure dimensions are even
            zoom,
            +width.toFixed(4),
            +height.toFixed(4)
        )
    }

    /**
     * Returns a string representation of the object to be used for Mapbox api, or help documentation.
     *
     * @example
     * ImageSize.fromSize(100,200).toString()       // s1x2
     * ImageSize.fromSize(100,200).toString(true)   // s1x2
     * ImageSize.fromSize(100,200).toString(false)  // 1x2
     *
     * ImageSize.fromRatio(1,2).toString()         // r1x2
     * ImageSize.fromRatio(1,2).toString(true)     // r1x2
     * ImageSize.fromRatio(1,2).toString(false)    // 1x2
     *
     * ImageSize.fromRatio(1,2,3).toString()       // r1x2x3
     * ImageSize.fromRatio(1,2,3).toString(true)   // r1x2x3
     * ImageSize.fromRatio(1,2,3).toString(false)  // 1x2x3
     *
     * ImageSize.fromSize(100,200,3).toString()       // s1x2x3
     * ImageSize.fromSize(100,200,3).toString(true)   // s1x2x3
     * ImageSize.fromSize(100,200,3).toString(false)  // 1x2x3
     *
     * @param prefix - Prefix the string with `r` for ratio or `s` for size [default: true]
     */
    toString(prefix = true) {
        return this.ratio ? `${prefix ? 'r' : ''}${this.ratio.width}x${this.ratio.height}${this.isZoom ? `x${this.zoom}` : ''}` : `${prefix ? 's' : ''}${this.width}x${this.height}${this.isZoom ? `x${this.zoom}` : ''}`
    }
}