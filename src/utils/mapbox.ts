import { TOKEN, USERNAME } from '/src/utils/config.ts'
import { ImageSize } from '/src/utils/image-size.ts'
import { JobStatus, RetrieveAStyleResponse } from '/src/utils/types.ts'

const API = 'https://api.mapbox.com'  // Root of the Mapbox API.

/**
 * This class just has static methods to access the Mapbox API.
 * Use it as follows:
 * @example
 * await Mapbox.someMethod()
 */
export class Mapbox {

    //#region Styles

    /**
     * @param styleId - The ID of the style to be retrieved.
     * @param user - The username of the account to which the style belongs.
     *
     * **[OPTIONAL]**
     *
     * **[DEFAULT]**=`USERNAME`
     *
     * In general, the default should be used,
     *   the exception is when you want to pull default Mapbox styles.
     * In this case, you would pass `mapbox` as the user.
     *
     * @returns A style as a JSON document.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/styles/#retrieve-a-style}
     * @privateRemarks Token Scope - styles:read
     */
    static async getStyle(styleId: string, user = USERNAME): Promise<RetrieveAStyleResponse> {
        const res = await fetch(`${API}/styles/v1/${user}/${styleId}?access_token=${TOKEN}`);

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return await res.json()
    }

    /**
     * @returns `Array<{id,name}>` containing all existing Mapbox Styles (name & id) for the user.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/styles/#list-styles}
     * @privateRemarks Token Scope - styles:list
     */
    static async getAllStyles() {
        const res = await fetch(`${API}/styles/v1/${USERNAME}?access_token=${TOKEN}&limit=${Date.now()}`);

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return (<Array<{ id: string, name: string }>>await res.json()).map(s => { return { id: s.id, name: s.name } })
    }

    /**
     * @param styleId - The ID of the style to be deleted.
     * @throws error if API response status is not 204
     * @see {@link https://docs.mapbox.com/api/maps/styles/#delete-a-style}
     * @privateRemarks Token Scope - styles:write
     */
    static async deleteStyle(styleId: string) {
        const res = await fetch(`${API}/styles/v1/${USERNAME}/${styleId}?access_token=${TOKEN}`, {
            method: "DELETE"
        });

        if (res.status !== 204) {
            throw new Error(await res.text());
        }
    }

    /**
     * @param styleObject - A style object conforming to https://docs.mapbox.com/api/maps/styles/#the-style-object which is used to create a Mapbox style.
     * @returns The id of the created style.
     * @throws error if API response status is not 201
     * @see {@link https://docs.mapbox.com/api/maps/styles/#create-a-style}
     * @privateRemarks Token Scope - styles:write
     */
    static async createStyle(styleObject: string) {
        const res = await fetch(`${API}/styles/v1/${USERNAME}?access_token=${TOKEN}`, {
            body: styleObject,
            headers: { 'Content-Type': 'application/json' },
            method: "POST"
        });

        if (res.status !== 201) {
            throw new Error(await res.text());
        }

        return (<{ id: string }>await res.json()).id
    }

    //#endregion Styles

    //#region Tilesets

    /**
     * @returns `Array<string>` containing all existing Mapbox Tileset ids for the user.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#list-tilesets}
     * @privateRemarks Token Scope - tilesets:list
     */
    static async getAllTilesets(): Promise<Array<string>> {
        const res = await fetch(`${API}/tilesets/v1/${USERNAME}?limit=100&access_token=${TOKEN}`);

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return (<Array<{ id: string }>>(await res.json())).map(t => t.id)
    }

    /**
     * @param tilesetId - The tileset's unique identifier of the tileset you want to delete.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#delete-tileset}
     * @privateRemarks Token Scope - tilesets:write
     */
    static async deleteTileset(tilesetId: string) {
        const res = await fetch(`${API}/tilesets/v1/${USERNAME}.${tilesetId}?access_token=${TOKEN}`, {
            method: "DELETE"
        });

        if (res.status !== 200) {
            throw new Error(await res.text());
        }
    }

    /**
     * @param tilesetName - The unique name of the tileset.  Used to generate the tileset ID which is `{username}.{tilesetName}`
     * @param body - The JSON string sent to the api.  e.g., `{ "recipe": ..., "name", ...}`
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#create-a-tileset}
     * @privateRemarks Token Scope - tilesets:write
     */
    static async createTileset(tilesetName: string, body: string) {
        const res = await fetch(`${API}/tilesets/v1/${USERNAME}.${tilesetName}?access_token=${TOKEN}`, {
            body,
            headers: { 'Content-Type': 'application/json' },
            method: "POST"
        });

        if (res.status !== 200) {
            throw new Error(await res.text());
        }
    }

    /**
     * @param tilesetName - The unique name of the tileset.  Used to generate the tileset ID which is `{username}.{tilesetName}`
     * @returns The ID of the job processing the tileset.  Used by {@link Mapbox.getJobStatus}.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#publish-a-tileset}
     * @privateRemarks Token Scope - tilesets:write
     */
    static async publishTileset(tilesetName: string) {
        const res = await fetch(`${API}/tilesets/v1/${USERNAME}.${tilesetName}/publish?access_token=${TOKEN}`, {
            method: "POST"
        });

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return (<{ jobId: string }>(await res.json())).jobId
    }

    /**
     * @param jobId - The publish job's unique identifier.
     * @param tilesetName - The unique name of the tileset.  Used to generate the tileset ID which is `{username}.{tilesetName}`
     * @returns The current status of the job.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#retrieve-information-about-a-single-tileset-job}
     * @privateRemarks Token Scope - tilesets:read
     */
    static async getJobStatus(jobId: string, tilesetName: string) {
        const res = await fetch(`${API}/tilesets/v1/${USERNAME}.${tilesetName}/jobs/${jobId}?access_token=${TOKEN}`);

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return (<{ stage: JobStatus }>await res.json()).stage
    }


    //#endregion Tilesets

    //#region Tileset Sources

    /**
     * @returns `Array<string>` containing all existing Mapbox Tileset Source ids for the user.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#list-tileset-sources}
     * @privateRemarks Token Scope - tilesets:list
     */
    static async getAllTilesetSources() {
        const res = await fetch(`${API}/tilesets/v1/sources/${USERNAME}?limit=500&access_token=${TOKEN}`);

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return (<Array<{ id: string }>>(await res.json())).map(t => t.id)
    }

    /**
     * Delete a tileset source.
     * @param id - The ID for the tileset source to be deleted.
     * @throws error if API response status is not 204
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#delete-a-tileset-source}
     * @privateRemarks Token Scope - tilesets:write
     */
    static async deleteTilesetSource(id: string) {
        const res = await fetch(`${API}/tilesets/v1/sources/${USERNAME}/${id}?access_token=${TOKEN}`, {
            method: "DELETE"
        });

        if (res.status !== 204) {
            throw new Error(await res.text());
        }
    }

    /**
     * Create (or update/replace) a tileset source.
     * @param id - The ID for the tileset source to be replaced.
     * @param body - String of line-delimited GeoJSON.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#replace-a-tileset-source}
     * @privateRemarks Token Scope - tilesets:write
     */
    static async createTilesetSource(id: string, body: FormData) {
        const res = await fetch(`${API}/tilesets/v1/sources/${USERNAME}/${id}?access_token=${TOKEN}`, {
            body,
            method: "PUT"
        });

        if (res.status !== 200) {
            throw new Error(await res.text());
        }
    }

    //#endregion Tileset Sources

    //#region Miscellaneous

    /**
     * Get the current number of items in the Mapbox job processing queue.
     * @returns The number of queued jobs.
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/mapbox-tiling-service/#view-the-global-queue}
     * @privateRemarks Token Scope - tilesets:read
     */
    static async getQueue() {
        const res = await fetch(`${API}/tilesets/v1/queue?access_token=${TOKEN}`, {
            method: "PUT"
        });

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return (<{ total: number }>await res.json()).total
    }

    /**
     * Get an image.
     * @param styleId - The ID of the style from which to create a static map.
     * @param size - Object that specifies the dimensions and whether bounding box or zoom.
     * @param position - The location string for either bounding box (e.g., [lon,lat,lon,lat]) or zoom (e.g., lon,lat).
     * @param padding - OPTIONAL A value specifying padding for the image.  Only applicable for bounding box images.
     * @returns The image as a `Uint8Array`
     * @throws error if API response status is not 200
     * @see {@link https://docs.mapbox.com/api/maps/static-images/#retrieve-a-static-map-from-a-style}
     * @privateRemarks Token Scope - styles:tiles
     */
    static async getImage(styleId: string, size: ImageSize, position: string, padding?: number) {
        const url = `${API}/styles/v1/${USERNAME}/${styleId}/static/${position}${size.isZoom ? `,${size.zoom}` : ''}/${size.width / (size.twoX ? 2 : 1)}x${size.height / (size.twoX ? 2 : 1)}${size.twoX ? '@2x' : ''}?access_token=${TOKEN}${padding ? `&padding=${padding}` : ''}&logo=false&attribution=false&before_layer=${Date.now()}`
        console.log(url)
        const res = await fetch(url, { cache: 'no-store' });

        if (res.status !== 200) {
            throw new Error(await res.text());
        }

        return new Uint8Array(await res.arrayBuffer())
    }

    //#endregion Miscellaneous

}
