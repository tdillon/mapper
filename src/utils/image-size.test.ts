import { assert, assertEquals, assertThrows } from 'std/assert/mod.ts'
import { ImageSize } from '/src/utils/image-size.ts'

//#region fromSize

[
    { w: 1, h: 1, twoX: false },
    { w: 11, h: 1, twoX: false },
    { w: 11, h: 2, twoX: false },
    { w: 11, h: 3, twoX: false },
    { w: 11, h: 4, twoX: false },
    { w: 11, h: 5, twoX: false },
    { w: 11, h: 6, twoX: false },
    { w: 11, h: 7, twoX: false },
    { w: 11, h: 8, twoX: false },
    { w: 11, h: 9, twoX: false },
    { w: 11, h: 10, twoX: false },
    { w: 1, h: 11, twoX: false },
    { w: 2, h: 11, twoX: false },
    { w: 3, h: 11, twoX: false },
    { w: 4, h: 11, twoX: false },
    { w: 5, h: 11, twoX: false },
    { w: 6, h: 11, twoX: false },
    { w: 7, h: 11, twoX: false },
    { w: 8, h: 11, twoX: false },
    { w: 9, h: 11, twoX: false },
    { w: 10, h: 11, twoX: false },
    { w: 1280, h: 1280, twoX: false },
    { w: 1280, h: 1279, twoX: false },
    { w: 1280, h: 1278, twoX: false },
    { w: 1280, h: 1277, twoX: false },
    { w: 1279, h: 1280, twoX: false },
    { w: 1278, h: 1280, twoX: false },
    { w: 1277, h: 1280, twoX: false },
    { w: 1282, h: 1280, twoX: true },
    { w: 1280, h: 1282, twoX: true },
    { w: 2560, h: 2, twoX: true },
    { w: 2, h: 2560, twoX: true },
    { w: 2560, h: 1278, twoX: true },
    { w: 1278, h: 2560, twoX: true },
].forEach(size =>
    Deno.test(`valid size - [size of ${size.w}x${size.h} and @2x == ${size.twoX}]`, () => {
        const x = ImageSize.fromSize(size.w, size.h)
        assertEquals(x.width, size.w)
        assertEquals(x.height, size.h)
        assertEquals(x.twoX, size.twoX)
        assert(x.isBoundingBox)
        assert(!x.isZoom)
        assert(typeof x.zoom === 'undefined')
        assertEquals(x.toString(), `s${size.w}x${size.h}`)
        assertEquals(x.toString(false), `${size.w}x${size.h}`)
    })
)

Deno.test('invalid size should error - [< 1]', () => [
    { w: 1, h: 0 },
    { w: 0, h: 1 },
    { w: 0, h: 0 },

    { w: -1, h: 1 },
    { w: 1, h: -1 },
    { w: -1, h: -1 },
].forEach(size => assertThrows(() => ImageSize.fromSize(size.w, size.h))))

Deno.test('invalid size should error - [decimals]', () => [
    { w: 1, h: 1.1 },
    { w: 1.1, h: 1 },
    { w: 1.1, h: 1.1 },
].forEach(size => assertThrows(() => ImageSize.fromSize(size.w, size.h))))

Deno.test('invalid size should error - [> 2560]', () => [
    { w: 1, h: 2561 },
    { w: 2561, h: 1 },

    { w: 2, h: 2561 },
    { w: 2561, h: 2 },

    { w: 1280, h: 2561 },
    { w: 2561, h: 1280 },

    { w: 2560, h: 2561 },
    { w: 2561, h: 2560 },

    { w: 2561, h: 2561 },
].forEach(size => assertThrows(() => ImageSize.fromSize(size.w, size.h))))

Deno.test('invalid size should error - [must be even when > 1280]', () => [
    { w: 2, h: 1281 },
    { w: 1281, h: 2 },

    { w: 1284, h: 1281 },
    { w: 1281, h: 1284 },

    { w: 1279, h: 2560 },
    { w: 2560, h: 1279 },

    { w: 2560, h: 1281 },
    { w: 1281, h: 2560 },
].forEach(size => assertThrows(() => ImageSize.fromSize(size.w, size.h))));

Deno.test('invalid zoom - [from size]', () => [
    -1, 22.1
].forEach(zoom => assertThrows(() => ImageSize.fromSize(1, 1, zoom))));

Deno.test('valid zoom - [from size]', () => [
    0, .1, 21.9, 22
].forEach(zoom => {
    const x = ImageSize.fromSize(1, 1, zoom)
    assertEquals(x.zoom, zoom)
    assert(x.isZoom)
    assert(!x.isBoundingBox)
    assert(!x.twoX)
    assertEquals(x.toString(), `s1x1x${zoom}`)
    assertEquals(x.toString(false), `1x1x${zoom}`)
}));

//#endregion fromSize

//#region fromRatio

[
    { w: 2560, h: 2560, rw: 1, rh: 1 },
    { w: 2560, h: 1280, rw: 2, rh: 1 },
    { w: 1280, h: 2560, rw: 1, rh: 2 },
    { w: 2560, h: 2304, rw: 1.111, rh: 1 },
    { w: 1978, h: 2560, rw: 8.5, rh: 11 },
    { w: 2560, h: 1978, rw: 11, rh: 8.5 },
    { w: 2048, h: 2560, rw: 8, rh: 10 },
    { w: 2560, h: 2048, rw: 10, rh: 8 },
    { w: 2560, h: 426, rw: 6, rh: 1 },
].forEach(ratio =>
    Deno.test(`valid ratios - [ratio of ${ratio.rw}x${ratio.rh} gives size ${ratio.w}x${ratio.h} and 2x]`, () => {
        const x = ImageSize.fromRatio(ratio.rw, ratio.rh)
        assertEquals(x.width, ratio.w)
        assertEquals(x.height, ratio.h)
        assert(x.twoX)
        assert(x.isBoundingBox)
        assert(!x.isZoom)
        assert(typeof x.zoom === 'undefined')
        assertEquals(x.toString(), `r${ratio.rw}x${ratio.rh}`)
        assertEquals(x.toString(false), `${ratio.rw}x${ratio.rh}`)
    })
)

Deno.test('invalid ratios should error - [<= 0]', () => [
    { w: 0, h: 0 },
    { w: 0, h: 1 },
    { w: 1, h: 0 },
    { w: -1, h: -1 },
    { w: -1, h: 1 },
    { w: 1, h: -1 },
].forEach(ratio => assertThrows(() => ImageSize.fromRatio(ratio.w, ratio.h))))

Deno.test('invalid ratios should error - [more than 4 decimal places]', () => [
    { w: 1.00001, h: 1 },
    { w: 1, h: 1.00001 },
    { w: 1.00001, h: 1.00001 },

    { w: .00001, h: 1 },
    { w: 1, h: .00001 },
    { w: .00001, h: .00001 },

    { w: 2560.00001, h: 1 },
    { w: 1, h: 2560.00001 },
    { w: 2560.00001, h: 2560.00001 },
].forEach(ratio => assertThrows(() => ImageSize.fromRatio(ratio.w, ratio.h))));

Deno.test('invalid zoom - [from Ratio]', () => [
    -1, 22.1
].forEach(zoom => assertThrows(() => ImageSize.fromRatio(1, 1, zoom))));

Deno.test('valid zoom - [from Ratio]', () => [
    0, .1, 21.9, 22
].forEach(zoom => {
    const x = ImageSize.fromRatio(1, 1, zoom)
    assertEquals(x.zoom, zoom)
    assert(x.isZoom)
    assert(!x.isBoundingBox)
    assertEquals(x.toString(), `r1x1x${zoom}`)
    assertEquals(x.toString(false), `1x1x${zoom}`)
}));

//#endregion fromRatio